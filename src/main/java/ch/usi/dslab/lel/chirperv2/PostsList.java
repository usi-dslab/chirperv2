/*

 chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;

import ch.usi.dslab.lel.dynastarv2.probject.ObjId;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class PostsList implements Serializable {
    public static final int RECENT_POSTS_COUNT = 10;
    List<Post> postsList;

    public PostsList() {
        postsList = Collections.synchronizedList(new LinkedList<Post>());
    }

    public PostsList(List postsList) {
        this.postsList = postsList;
    }

    public static int hashPostsList(List<Post> postsList) {
        int plHash = 0;
        for (Post post : postsList)
            plHash ^= post.hashCode();
        return plHash;
    }

    public List<Post> getRawList() {
        return postsList;
    }

    public boolean containsPost(Post post) {
        return postsList.contains(post);
    }

    public void addPost(Post post) {
        // don't add a post twice
        // TODO: re-enable this. turn off for performance
//        if (postsList.contains(post))
//            return;

        // insert new posts at the beginning
        postsList.add(0, post);

        // put the posts in the timeline in order
        // TODO: re-enable this. turn off for performance
//      Collections.sort(postsList, Post.comparatorNewestFirst);

        // put old posts somewhere...
        moveOldPostsToDisk();
    }

    public void removePostsFromUser(ObjId userId) {
        Iterator<Post> it = postsList.iterator();
        while (it.hasNext()) {
            Post p = it.next();
            if (p.getPosterId() == userId)
                it.remove();
        }
    }

    private void moveOldPostsToDisk() {
        //discard old posts (they actually should be flushed to disk or something...)
        while (postsList.size() > RECENT_POSTS_COUNT)
            postsList.remove(postsList.size() - 1);
    }

    public void print() {
        System.out.println(String.format("Posts list (%d posts):", postsList.size()));
        for (Post post : postsList)
            System.out.println("   " + post);
    }

    @Override
    public int hashCode() {
        return hashPostsList(postsList);
    }

    @Override
    public boolean equals(Object other_) {
        PostsList other = (PostsList) other_;
        return this.postsList.equals(other.postsList);
    }

    @Override
    public String toString() {
        return "[POSTS]";
    }
}
