/*

 Chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.benchmarks;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.chirperv2.AppCommandType;
import ch.usi.dslab.lel.chirperv2.ChirperClient;
import ch.usi.dslab.lel.chirperv2.User;
import ch.usi.dslab.lel.chirperv2.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chirperv2.util.Util;
import ch.usi.dslab.lel.dynastarv2.EvenCallBack;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

public class TestRunnerDynamic implements TestRunnerInterface {
    private static double skew = 1.2d;
    Random randomGenerator;

    ChirperClientInterface client;
    Semaphore sendPermits;
    int numUsers;
    long clientId;
    double totalWeight;
    int totalChance;
    double postWeight;
    double followWeight;
    double unfollowWeight;
    double getTimelineWeight;
    BiConsumer callbackHandler;
    Set<SocialNetworkGenerator.RandomUser> socialNetwork;
    Map<Integer, SocialNetworkGenerator.RandomUser> users = new ConcurrentHashMap<>();
    Map<Integer, SocialNetworkGenerator.RandomUser> availableUsersForPost = new ConcurrentHashMap<>();
    Queue<FollowEdge> availableEdge = new ConcurrentLinkedQueue<>();
    Map<Integer, SocialNetworkGenerator.RandomUser> remainingUsers = new ConcurrentHashMap<>();
    int partitionNum = 0;
    boolean linked = false;
    final boolean preloadNetwork;


    ThroughputPassiveMonitor queryMonintor;
    ThroughputPassiveMonitor retryPrepareMonitor;
    ThroughputPassiveMonitor retryCommandMonitor;
    ThroughputPassiveMonitor localCommandMonitor;
    ThroughputPassiveMonitor globalCommandMonitor;
    ThroughputPassiveMonitor messageDropMonitor;
    int minId = 0;
    int maxId = 1000;
    boolean stopCreating = false;
    boolean stopFollowing = false;
    AtomicInteger edgesCreate = new AtomicInteger(0);
    AtomicInteger totalEdges = new AtomicInteger(0);
    AtomicInteger usersCreate = new AtomicInteger(0);

    public TestRunnerDynamic(ChirperClientInterface client, int clientId, int numPermits, String socialNetworkFile,
                             double postProbability, double followProbability, double unfollowProbability,
                             double getTimelineProbability, boolean preloadNetwork, int minId, int maxId) {

        randomGenerator = new Random(System.nanoTime());
        this.client = client;
        this.preloadNetwork = preloadNetwork;
        this.minId = minId;
        this.maxId = maxId;

        sendPermits = new Semaphore(numPermits);
        socialNetwork = Util.loadSocialNetwork(socialNetworkFile);
        String[] params = socialNetworkFile.split("_");
        if (socialNetworkFile.indexOf("Linked") > 0) linked = true;
//        linked = true;
        String[] tmp = params[params.length - 1].split("\\.");
        partitionNum = Integer.parseInt(tmp[0]);


        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            users.put((int) user.userId, user);
            if (user.userId >= minId && user.userId < maxId) {
                remainingUsers.put(user.userId, user);
//                originMap.put(user.userId, user);
            }
        }
        this.numUsers = socialNetwork.size();

        remainingUsers = sortByNodeDegree(remainingUsers, true);
        postWeight = postProbability;
        followWeight = followProbability;
        unfollowWeight = unfollowProbability;
        getTimelineWeight = getTimelineProbability;

        totalWeight = postWeight + followWeight + unfollowWeight + getTimelineWeight;

        callbackHandler = new DynamicBenchCallbackHandlerWithContext(this, clientId);
        this.clientId = clientId;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        int experimentDuration = Integer.parseInt(args[0]);
        String fileDirectory = args[1];
        String gathererAddress = args[2];
        int gathererPort = Integer.parseInt(args[3]);
        int warmUpTime = Integer.parseInt(args[4]);
        int numPermits = Integer.parseInt(args[5]);
        String socialNetworkFile = args[6];
        double postWeight = Double.parseDouble(args[7]);
        double followWeight = Double.parseDouble(args[8]);
        double unfollowWeight = Double.parseDouble(args[9]);
        double getTimelineWeight = Double.parseDouble(args[10]);
        boolean preloadedData = Boolean.parseBoolean(args[11]);
        boolean legacySSMR = Boolean.parseBoolean(args[12]);
        int minId = Integer.parseInt(args[13]) - 1;
        int maxId = Integer.parseInt(args[14]);
        String[] clientArgs = Arrays.copyOfRange(args, 15, args.length);
        int clientId = Integer.parseInt(args[15]);

        ChirperClientInterface client = getClientInterface(numPermits, clientArgs);

//      boolean hasOptimistic = implementation.equals("chirperBASELINE") || implementation.equals("chirperPREFETCH");
        if (client instanceof ChirperBenchClient) {
//            ((ChirperBenchClient) client).loadSocialNetworkIntoCache(socialNetworkFile);
        }

        // ====================================
        // setting up monitoring infrastructure
        DataGatherer.configure(experimentDuration, fileDirectory, gathererAddress, gathererPort, warmUpTime);
        // ====================================


        TestRunnerDynamic runner = new TestRunnerDynamic(client, clientId, numPermits, socialNetworkFile,
                postWeight, followWeight, unfollowWeight, getTimelineWeight, preloadedData, minId, maxId);

//        runner.moveMonitor = new ThroughputPassiveMonitor(clientId, "client_move_rate", true);
//        runner.retryPrepareMonitor = new ThroughputPassiveMonitor(clientId, "client_retry_prepare_rate", true);
        runner.retryCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_retry_command_rate", true);
        runner.queryMonintor = new ThroughputPassiveMonitor(clientId, "client_query_rate", true);
//        runner.localCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_local_command_rate", true);
//        runner.globalCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_global_command_rate", true);
//        runner.messageDropMonitor = new ThroughputPassiveMonitor(clientId, "client_message_drop_rate", true);

        if (client instanceof ChirperBenchClient) {

            ((ChirperBenchClient) client).chirperClient.proxyClient.registerMonitoringEvent(
                    new MonitorCallBack(runner.queryMonintor),
//                    new MonitorCallBack(runner.moveMonitor),
//                    new MonitorCallBack(runner.retryPrepareMonitor),
                    new MonitorCallBack(runner.retryCommandMonitor)
//                    new MonitorCallBack(runner.localCommandMonitor),
//                    new MonitorCallBack(runner.globalCommandMonitor),
//                    new MonitorCallBack(runner.messageDropMonitor)
            );
        }

        // actually run (this client's part of) the experiment
        runner.runTests(experimentDuration);

        // wait for the monitors to finish logging and sendind data to the DataGatherer

    }

    private static Map<Integer, SocialNetworkGenerator.RandomUser> sortByNodeDegree(Map<Integer, SocialNetworkGenerator.RandomUser> unsortMap, final boolean order) {

        List<Map.Entry<Integer, SocialNetworkGenerator.RandomUser>> list = new LinkedList<>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, (e1, e2) -> ((Integer) e1.getValue().followers.size()).compareTo(e2.getValue().followers.size()));

        // Maintaining insertion order with the help of LinkedList
        Map<Integer, SocialNetworkGenerator.RandomUser> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<Integer, SocialNetworkGenerator.RandomUser> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }


    static class MonitorCallBack implements EvenCallBack {
        ThroughputPassiveMonitor monitor;

        public MonitorCallBack(ThroughputPassiveMonitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void callback(Object data) {
            monitor.incrementCount((Integer) data);
        }
    }


    static ChirperClientInterface getClientInterface(int numPermits, String... args) {
        int clientId = Integer.parseInt(args[0]);
        String implementation = args[1];
        if (implementation.contains("CHITTER")) {
            String systemConfigFile = args[2];
            String partitioningFile = args[3];
            ChirperClient chirperClient = new ChirperClient(clientId, systemConfigFile, partitioningFile, numPermits);
            return new ChirperBenchClient(chirperClient);
        }
        return null;
    }

    public void runTests(int durationSecs) {
        long start = System.currentTimeMillis();
        long end = start + durationSecs * 1000;
        Date startTime = new Date(start);
        Date endTime = new Date(end);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        System.out.println("Client " + this.clientId + " start at " + formatter.format(startTime)
                + " and expected to end at " + formatter.format(endTime));
        while (System.currentTimeMillis() < end)
            sendOneCommand();
        try {
            Thread.sleep(10 * durationSecs * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Client " + this.clientId + " stopped at " + formatter.format(new Date(System.currentTimeMillis())));
            System.exit(1);
        }
    }

    void createUser() {
        BenchCallbackContext context = new BenchCallbackContext(AppCommandType.NEWUSER);
        if (remainingUsers.size() == 0 && !stopCreating) {
            System.out.println(this.clientId + " >> # user stop creating at " + usersCreate.get() + " out of " + (maxId - minId + 1));
            stopCreating = true;
            return;
        }
        List<Integer> tmp = new ArrayList<>(remainingUsers.keySet());
        try {
            SocialNetworkGenerator.RandomUser user = remainingUsers.remove(remainingUsers.keySet().iterator().next());
            client.createUser(user.userId, callbackHandler, context);
            usersCreate.incrementAndGet();
//            System.out.println(this.clientId + " >> # " + usersCreate + " - created user " + user.userId + " with follower size " + user.followers.size());
            availableUsersForPost.put(user.userId, user);
            for (int i : user.followers) {
                availableEdge.add(new FollowEdge(user.userId, i));
            }
            totalEdges.addAndGet(user.followers.size());
        } catch (NoSuchElementException e) {
            stopCreating = true;
            return;
        }
    }

    public void sendOneCommand() {
        getPermit();

        double createWeight = (1 - ((float) (availableUsersForPost.size()) / (maxId - minId + 1))) * totalWeight;
        createWeight *= 0.01;

        double randDouble = randomGenerator.nextDouble() * totalWeight;

        if (randDouble <= createWeight && remainingUsers.size() > 0) {
            // generate newUserCommand
            createUser();

            if (!stopCreating) return;
        }

        if (randDouble <= postWeight || (stopCreating && stopFollowing)) {
            List userIds = new ArrayList(availableUsersForPost.keySet());
            if (userIds.size() == 0) {
                releasePermit();
                return;
            }
            int userId = (int) userIds.get(randomGenerator.nextInt(userIds.size()));
            SocialNetworkGenerator.RandomUser user = availableUsersForPost.get(userId);
            // generate postCommand
            byte[] post = new byte[1];
            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.POST);
            client.post(userId, post, callbackHandler, context);
            return;
        }
        randDouble -= postWeight;

        if (randDouble <= followWeight && !stopFollowing) {
//            System.out.println(this.clientId+" >> #"+"Begin follow");
            FollowEdge followEdge = null;
            if (availableEdge.size() > 0) {
                followEdge = availableEdge.poll();
            }
            if (followEdge == null) {
//                System.out.println(this.clientId+" >> #"+"can't pick any, try to create");
                createUser();
                if (stopCreating && availableEdge.size() == 0) {
                    System.out.println(this.clientId + " >> # user stop following at " + edgesCreate.incrementAndGet() + " out of " + totalEdges);
                    stopFollowing = true;
                }
                releasePermit();
                return;
            }
            FollowEdge finalFollowEdge = followEdge;
            int followerId = followEdge.followerId;
            int followedId = followEdge.followedId;
//            System.out.println(this.clientId+" >> #"+" user " + followerId + " is going to follow " + followedId);
            CompletableFuture<Message> future = new CompletableFuture<>();
            future.thenAccept(reply -> {
                if (reply.getNext().equals("OK")) {
                    edgesCreate.incrementAndGet();
//                    System.out.println(this.clientId+" >> #"+edgesCreate.get()+" - user " + followerId + " is followed " + followedId+" [(" + followerId + ")/(" + followedId + ")]");
                    availableEdge.remove(finalFollowEdge);
                    ((ChirperBenchClient) client).updateFollowerCache(followedId, followerId, true);
                } else {
                    availableEdge.remove(finalFollowEdge);
                    availableEdge.add(finalFollowEdge);
//                    System.out.println(this.clientId+" >> #"+"user " + followerId + " is not available");
                }
            }).exceptionally(throwable -> {
                throwable.printStackTrace();
                return null;
            });
            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.FOLLOW, future);
            client.follow(followerId, followedId, callbackHandler, context);
            return;
        }
        randDouble -= followWeight;
        releasePermit();
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void releasePermit() {
        sendPermits.release();
    }

    private class FollowEdge {
        public int followerId;
        public int followedId;

        public FollowEdge(int followedId, int followerId) {
            this.followedId = followedId;
            this.followerId = followerId;
        }
    }

}
