/*

 Chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.benchmarks;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ObjectCountPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.chirperv2.AppCommandType;
import ch.usi.dslab.lel.chirperv2.ChirperClient;
import ch.usi.dslab.lel.chirperv2.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chirperv2.util.Util;
import ch.usi.dslab.lel.dynastarv2.EvenCallBack;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;

public class TestRunner implements TestRunnerInterface {
    Random randomGenerator;

    ChirperClientInterface client;
    Semaphore sendPermits;
    int numUsers;
    long clientId;
    double totalWeight;
    int totalChance;
    double postWeight;
    double followWeight;
    double unfollowWeight;
    double getTimelineWeight;
    BiConsumer callbackHandler;
    Set<SocialNetworkGenerator.RandomUser> socialNetwork;
    Map<Integer, SocialNetworkGenerator.RandomUser> users = new HashMap<>();

    int partitionNum = 0;
    boolean linked = false;

    ThroughputPassiveMonitor moveMonitor;
    ThroughputPassiveMonitor queryMonintor;
    ThroughputPassiveMonitor retryPrepareMonitor;
    ThroughputPassiveMonitor retryCommandMonitor;
    ThroughputPassiveMonitor localCommandMonitor;
    ThroughputPassiveMonitor globalCommandMonitor;
    ThroughputPassiveMonitor messageDropMonitor;
    ObjectCountPassiveMonitor objectMonitor;


    public TestRunner(ChirperClientInterface client, int clientId, int numPermits, String socialNetworkFile,
                      double postProbability, double followProbability, double unfollowProbability,
                      double getTimelineProbability) {

        randomGenerator = new Random(System.nanoTime());
        this.client = client;

        sendPermits = new Semaphore(numPermits);
        socialNetwork = Util.loadSocialNetwork(socialNetworkFile);
        String[] params = socialNetworkFile.split("_");
        if (socialNetworkFile.indexOf("Linked") > 0) linked = true;
//        linked = true;
        String[] tmp = params[params.length - 1].split("\\.");
        partitionNum = Integer.parseInt(tmp[0]);
        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            users.put((int) user.userId, user);
        }
        this.numUsers = socialNetwork.size();
        postWeight = postProbability;
        followWeight = followProbability;
        unfollowWeight = unfollowProbability;
        getTimelineWeight = getTimelineProbability;

        totalWeight = postWeight + followWeight + unfollowWeight + getTimelineWeight;

        callbackHandler = new TestRunner.BenchCallbackHandlerWithContext(this, clientId);
        this.clientId = clientId;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        int index = 0;
        int numPermits = Integer.parseInt(args[index++]);
        String socialNetworkFile = args[index++];
        String gathererNode = args[index++];
        int gathererPort = Integer.parseInt(args[index++]);
        String fileDirectory = args[index++];
        int experimentDuration = Integer.parseInt(args[index++]);
        int warmUpTime = Integer.parseInt(args[index++]);
        double postWeight = Double.parseDouble(args[index++]);
        double followWeight = Double.parseDouble(args[index++]);
        double unfollowWeight = Double.parseDouble(args[index++]);
        double getTimelineWeight = Double.parseDouble(args[index++]);
        String[] clientArgs = Arrays.copyOfRange(args, index, args.length);
        int clientId = Integer.parseInt(args[index]);

        ChirperClientInterface client = getClientInterface(numPermits, clientArgs);


        if (client instanceof ChirperBenchClient) {
            ((ChirperBenchClient) client).loadSocialNetworkIntoCache(socialNetworkFile);
        }

        // ====================================
        // setting up monitoring infrastructure
        DataGatherer.configure(experimentDuration, fileDirectory, gathererNode, gathererPort, warmUpTime);
        // ====================================


        TestRunner runner = new TestRunner(client, clientId, numPermits, socialNetworkFile,
                postWeight, followWeight, unfollowWeight, getTimelineWeight);

//        runner.moveMonitor = new ThroughputPassiveMonitor(clientId, "client_move_rate", true);
//        runner.retryPrepareMonitor = new ThroughputPassiveMonitor(clientId, "client_retry_prepare_rate", true);
        runner.retryCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_retry_command_rate", true);
        runner.queryMonintor = new ThroughputPassiveMonitor(clientId, "client_query_rate", true);
//        runner.localCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_local_command_rate", true);
//        runner.globalCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_global_command_rate", true);
//        runner.messageDropMonitor = new ThroughputPassiveMonitor(clientId, "client_message_drop_rate", true);
//        runner.objectMonitor = new ObjectCountPassiveMonitor(clientId, "client_permits_count", true);
        if (client instanceof ChirperBenchClient) {

            ((ChirperBenchClient) client).chirperClient.proxyClient.registerMonitoringEvent(
                    new MonitorCallBack(runner.queryMonintor),
//                    new MonitorCallBack(runner.moveMonitor),
//                    new MonitorCallBack(runner.retryPrepareMonitor),
                    new MonitorCallBack(runner.retryCommandMonitor)
//                    new MonitorCallBack(runner.localCommandMonitor),
//                    new MonitorCallBack(runner.globalCommandMonitor),
//                    new MonitorCallBack(runner.messageDropMonitor)
            );
        }

        // actually run (this client's part of) the experiment
        runner.runTests(experimentDuration);

        // wait for the monitors to finish logging and sendind data to the DataGatherer

    }

    static ChirperClientInterface getClientInterface(int numPermits, String... args) {
        int clientId = Integer.parseInt(args[0]);
//        String implementation = args[1];
//        if (implementation.contains("CHITTER")) {
        String systemConfigFile = args[1];
        String partitioningFile = args[2];
        ChirperClient chirperClient = new ChirperClient(clientId, systemConfigFile, partitioningFile, numPermits);
        return new ChirperBenchClient(chirperClient);
//        }
//        return null;
    }

    public void runTests(int durationSecs) {
        long start = System.currentTimeMillis();
        long end = start + durationSecs * 1000;
        Date startTime = new Date(start);
        Date endTime = new Date(end);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        System.out.println("Client " + this.clientId + " start at " + formatter.format(startTime)
                + " and expected to end at " + formatter.format(endTime));
        while (System.currentTimeMillis() < end) {
            sendOneCommand();
        }


        try {
            Thread.sleep(10 * durationSecs * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Client " + this.clientId + " stopped at " + formatter.format(new Date(System.currentTimeMillis())));
            System.exit(1);
        }
    }

    public void sendOneCommand() {
        getPermit();

        double randDouble = randomGenerator.nextDouble() * totalWeight;
//        int userId = uniformRandom ? randomGenerator.nextInt(numUsers) : getUserWithInterestRate();
        int userId = randomGenerator.nextInt(numUsers);
        if (userId < 0) throw new RuntimeException("Can't find user???");

//        // test getInfo
//        TestRunner.BenchCallbackContext context = new TestRunner.BenchCallbackContext(AppCommandType.FOLLOW_NOP);
//        client.getInfoNB(userId, callbackHandler, context);
//        return;

        if (randDouble <= postWeight) {

            // generate postCommand
            byte[] post = new byte[1];
            TestRunner.BenchCallbackContext context = new TestRunner.BenchCallbackContext(AppCommandType.POST);

            context.nodeSize = client.getUserFollowersCount(userId);
//            if (context.nodeSize > 50) {
//                releasePermit();
//                return;
//            }
            client.post(userId, post, callbackHandler, context);
            return;
        }
        randDouble -= postWeight;

        if (randDouble <= followWeight) {
            // generate followCommand
            int followerId = userId;
            int followedId = randomGenerator.nextInt(numUsers);
//            int followedId = linked ? (int) getUserByInterest(users.get(userId)).userId : randomGenerator.nextInt(numUsers);

            TestRunner.BenchCallbackContext context = new TestRunner.BenchCallbackContext(AppCommandType.FOLLOW_NOP);
            client.follow(followerId, followedId, callbackHandler, context);
            return;
        }
        randDouble -= followWeight;

        if (randDouble <= unfollowWeight) {
            // generate unfollowCommand
            int followerId = userId;
            int followedId = randomGenerator.nextInt(numUsers);
//            int followedId = linked ? (int) getUserByInterest(users.get(userId)).userId : randomGenerator.nextInt(numUsers);
            TestRunner.BenchCallbackContext context = new TestRunner.BenchCallbackContext(AppCommandType.UNFOLLOW_NOP);
            client.unfollow(followerId, followedId, callbackHandler, context);
            return;
        }
        randDouble -= unfollowWeight;

        if (randDouble <= getTimelineWeight) {
            // generate getTimelineCommand
            TestRunner.BenchCallbackContext context = new TestRunner.BenchCallbackContext(AppCommandType.GETTIMELINE);
            client.getTimeline(userId, callbackHandler, context);
            return;
        }
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void releasePermit() {
        sendPermits.release();
    }

    static class MonitorCallBack implements EvenCallBack {
        ThroughputPassiveMonitor monitor;

        public MonitorCallBack(ThroughputPassiveMonitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void callback(Object data) {
            monitor.incrementCount((Integer) data);
        }
    }

}
