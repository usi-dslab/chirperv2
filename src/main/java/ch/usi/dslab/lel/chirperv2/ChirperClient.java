/*

 Chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 * <p>
 * (18/08/2016) - Enrique Fynn: Added legacy SSMR support
 */

package ch.usi.dslab.lel.chirperv2;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.chirperv2.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chirperv2.util.Util;
import ch.usi.dslab.lel.dynastarv2.Client;
import ch.usi.dslab.lel.dynastarv2.command.Command;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

public class ChirperClient {
    public static final Logger log = LoggerFactory.getLogger(ChirperClient.class.toString());
    public boolean ready = false;
    public Client proxyClient;
    private Semaphore sendPermits;
    private Random rand = new Random(System.currentTimeMillis());
    private Map<ObjId, SocialNetworkGenerator.RandomUser> users = new HashMap<>();

    public ChirperClient(long clientId, String systemConfigFile, String partitioningFile) {
        this(clientId, systemConfigFile, partitioningFile, Integer.MAX_VALUE);
    }

    public ChirperClient(long clientId, String systemConfigFile, String partitioningFile, int numPermits) {
        proxyClient = new Client((int) clientId, systemConfigFile, partitioningFile);
        sendPermits = new Semaphore(numPermits);
    }

    public static void main(String[] args) {

        log.info("Entering ChirperClient.main(...)");
        int clientId = Integer.parseInt(args[0]);
        String systemConfigFile = args[1];
        String partitionsConfigFile = args[2];
        int numPermits = Integer.parseInt(args[3]);
        String socialNetworkFile = args[4];
        ChirperClient chirperClient = new ChirperClient(clientId, systemConfigFile, partitionsConfigFile, numPermits);
//        chirperClient.loadSocialNetworkIntoCache(socialNetworkFile);
        chirperClient.runInteractive(chirperClient);

    }

    void addSendPermit() {
        sendPermits.release();
    }

    public int getId() {
        return proxyClient.getId();
    }

    void getSendPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void loadSocialNetworkIntoCache(String file) {
        Util.loadSocialNetwork(file, this.proxyClient.getCache());
    }

    public void updateFollowerCache(ObjId userId, ObjId followerToAdd, boolean isAdding) {
        FollowersCache followersCache = FollowersCache.getCacheForUser(userId);
        Set<ObjId> followers = new HashSet<>();
        followers.add(followerToAdd);
        if (isAdding) followersCache.addFollowers(followers);
        else followersCache.removeFollowers(followers);
    }


    // ================================
    // ChirperClient User API
    // ================================

    void sendTTYCommand(String cmdtype, int arg1, int uid2, String data) {
        if (!cmdtype.equalsIgnoreCase("burstpost") && !cmdtype.equalsIgnoreCase("bursttl")) {
            getSendPermit();
        }

        System.out.println(String.format("Sending command %s %d %d %s", cmdtype, arg1, uid2, data));

        if (cmdtype.equalsIgnoreCase("new")) {
            Consumer newcbh = (message) -> {
                addSendPermit();
                Message reply = (Message) message;
                User user = (User) reply.getItem(0);
                FollowersCache userFollowerCache = FollowersCache.getCacheForUser(user.getId());
                userFollowerCache.setCache(user.getFollowerList().getRawList());
                System.out.println("(CONS) Successfully created User " + user.getId());
            };
            createUserNB(User.genObjId(arg1), newcbh);
        } else if (cmdtype.equalsIgnoreCase("f")) {
            Consumer followcbh = (message) -> {
                addSendPermit();
                updateFollowerCache(User.genObjId(uid2),User.genObjId(arg1), true);
                System.out.println("(CONS) Successfully done: " + message);
            };
            followNB(User.genObjId(arg1), User.genObjId(uid2), followcbh);
        } else if (cmdtype.equalsIgnoreCase("uf")) {
            Consumer unfollowcbh = (message) -> {
                addSendPermit();
                updateFollowerCache(User.genObjId(arg1), User.genObjId(uid2), false);
                System.out.println("(CONS) Successfully done: " + message);
            };

            unfollowNB(User.genObjId(arg1), User.genObjId(uid2), unfollowcbh);
        } else if (cmdtype.equalsIgnoreCase("p")) {
            Consumer postcbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };

            postNB(User.genObjId(arg1), new Post((long) proxyClient.getId(), User.genObjId(arg1), System.currentTimeMillis(), data.getBytes()), postcbh);
        } else if (cmdtype.equalsIgnoreCase("tl")) {
            Consumer timelinecbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };

            getTimelineNB(User.genObjId(arg1), timelinecbh);
        } else if (cmdtype.equalsIgnoreCase("pl")) {
            Consumer postslistscbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getPostsNB(User.genObjId(arg1), postslistscbh);
        } else if (cmdtype.equalsIgnoreCase("fr")) {
            Consumer followerscbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getFollowersNB(User.genObjId(arg1), followerscbh);
        } else if (cmdtype.equalsIgnoreCase("fd")) {
            Consumer followedschb = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getFollowedsNB(User.genObjId(arg1), followedschb);
        } else if (cmdtype.equalsIgnoreCase("burstpost")) {
            for (int i = 0; i < arg1; i++) {
                int puid1 = rand.nextInt(1000);
                String postdata = "qwertyuiopasdfghjkl;zxcvbnm";
                sendTTYCommand("p", puid1, 0, postdata);
            }
        } else if (cmdtype.equalsIgnoreCase("bursttl")) {
            for (int i = 0; i < arg1; i++) {
                int tluid1 = rand.nextInt(1000);
                sendTTYCommand("tl", tluid1, 0, null);
            }
        } else {
            System.err.println("wrong command type");
        }
    }

    public void createUserNB(ObjId userId, Consumer then) {
        log.info("Calling createUserNB for user id {}.", userId);
        proxyClient.create(new User(userId)).thenAccept(then);
    }

    public int getUserFollowersCount(int id) {
        ObjId userId = User.genObjId(id);
        return FollowersCache.getCacheForUser(userId).getFollowersCache().size();
    }

    public void postNB(ObjId userId, Post post, Consumer then) {
        log.info("User {} posting {}.", userId, post);
        Set<ObjId> estimatedFollowers = FollowersCache.getCacheForUser(userId).getFollowersCache();
        //TODO: rollback for follow/unfollow
//        Command command = new Command(AppCommandType.POST, estimatedFollowers, userId, post);
        Command command = new Command(AppCommandType.POST, userId, post, estimatedFollowers);
//        command.setInvolvedObjects(estimatedFollowers);
        Consumer postReplyHandler = new PostReplyHandler(then, this, userId, post);

        proxyClient.executeCommand(command).thenAccept(postReplyHandler);
    }

    public void followNB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} followNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.FOLLOW, followerId, followedId);
        proxyClient.executeCommand(command).thenAccept(then);
    }


    public void unfollowNB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.UNFOLLOW, followerId, followedId);

        proxyClient.executeCommand(command).thenAccept(then);
    }


    public void getTimelineNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their.", userId);
        Command command = new Command(AppCommandType.GETTIMELINE, userId);

        proxyClient.executeCommand(command).thenAccept(then);
    }

    public void getPostsNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their posts.", userId);
        Command command = new Command(AppCommandType.GETPOSTS, userId);

        proxyClient.executeCommand(command).thenAccept(then);
    }

    public void getFollowersNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their followers.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWERLIST, userId);

        proxyClient.executeCommand(command).thenAccept(then);
    }

    public void getFollowedsNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their followeds.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWEDLIST, userId);

        proxyClient.executeCommand(command).thenAccept(then);
    }

    public void runInteractive(ChirperClient chirperClient) {
        Scanner scan = new Scanner(System.in);
        String input;

        System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            try {
                String[] parts = input.split(" ");
                String cmdtype = parts[0];
                int arg1 = 0;
                int uid2 = 0;
                String data = null;

                boolean validCommand = true;

                if (cmdtype.equalsIgnoreCase("new")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("p")) {
                    arg1 = Integer.parseInt(parts[1]);
                    data = parts[2];
                } else if (cmdtype.equalsIgnoreCase("f")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("uf")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("tl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fr")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fd")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("pl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("burstpost")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("bursttl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else {
                    System.out.println("Invalid command.");
                    validCommand = false;
                }

                if (validCommand)
                    chirperClient.sendTTYCommand(cmdtype, arg1, uid2, data);

                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            } catch (Exception e) {
                System.err.println("Probably invalid input.");
                e.printStackTrace();
                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            }

            input = scan.nextLine();

        }
        scan.close();
    }


    public static class PostReplyHandler implements Consumer {
        Consumer userHandler;
        //        Object userContext;
        ChirperClient client;
        ObjId userId;
        Post post;
        // the command's followerCache never decreases, ensuring termination of the request execution
        FollowersCache commandFollowerCache;
        FollowersCache clientFollowerCache;

        public PostReplyHandler(Consumer userHandler, ChirperClient client, ObjId userId, Post post) {
            this.userHandler = userHandler;
//            this.userContext = userContext;
            this.client = client;
            this.userId = userId;
            this.post = post;
            clientFollowerCache = FollowersCache.getCacheForUser(userId);
            commandFollowerCache = new FollowersCache();
            commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
        }

        @Override
        public void accept(Object message) {
            Message reply = (Message) message;
            reply.rewind();
            String status = (String) reply.getNext();

            if (status.equals("DROPPED")) {
//                System.out.println("Received  reply for POST with status \"" + status + "\"");
                userHandler.accept(new Message("OK"));
                return;
            }
            Message replyIfOK = new Message("OK");
            replyIfOK.copyTimelineStamps(reply);

            Set<ObjId> followersToAdd = (Set<ObjId>) reply.getNext();
            Set<ObjId> followersToRemove = (Set<ObjId>) reply.getNext();
            clientFollowerCache.updateCache(followersToAdd, followersToRemove);

            if (status.equals("RETRY")) {
                System.out.println("WILL NOT COME TO THIS - Retrying post with more followers");
                commandFollowerCache.addFollowers(followersToAdd);
                Set<ObjId> estimatedFollowers = commandFollowerCache.getFollowersCache();
                Command command = new Command(AppCommandType.POST, userId, estimatedFollowers, post);
                client.proxyClient.executeCommand(command);
//                client.proxyClient.sendCommandAsync(this, userContext, command);
            } else { // if status.equal("OK")
//                System.out.println("handling POST reply with status \"" + status + "\"");
                userHandler.accept(replyIfOK);
            }


        }
    }
}
