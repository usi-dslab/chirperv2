package ch.usi.dslab.lel.chirperv2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Created by longle on 14.10.16.
 */
public class Utils {
    public static void loadCSVData(String filePath, FileReadLineCallback callback) {
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(callback::callback);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}