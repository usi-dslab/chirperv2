/*

 Chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano

 This file is part of Chirper.

 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastClient;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.lel.dynastarv2.Partition;
import ch.usi.dslab.lel.dynastarv2.command.Command;
import ch.usi.dslab.lel.dynastarv2.messages.DynaStarMessageType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ClientController {
    private  MulticastClient multicastClient;
    private int clientId = 999;
    private AtomicInteger requestId;

    ClientController(String partitioningFile, String systemConfigFile) {
        multicastClient = MulticastClientServerFactory.getClient(clientId, systemConfigFile);
        Partition.loadPartitions(partitioningFile);
        Partition.getOracleSet().forEach(this::connectToPartition);
        requestId = new AtomicInteger(0);
    }


    private void connectToPartition(Partition partition) {
        List<Integer> partitionServers = partition.getGroup().getMembers();
        int contactServerIndex = clientId % partitionServers.size();
        int contactServer = partitionServers.get(contactServerIndex);
        multicastClient.connectToServer(contactServer);
    }

    private void multicastQuery(Command command) {
        command.setId(clientId, requestId.getAndIncrement());
        Set<Group> destinationGroups = new HashSet<>();
        Set<Partition> dest = new HashSet<>();

        dest.addAll(Partition.getOracleSet());
        destinationGroups.addAll(dest.stream().map(Partition::getGroup).collect(Collectors.toList()));

        command.setDestinations(dest);
        ClientMessage commandMessage = new ClientMessage(DynaStarMessageType.DO_PARTITION, command);
        multicastClient.multicast(destinationGroups, commandMessage);
    }

    public static void main(String args[]) throws InterruptedException {
        //systemConfigFile, partitioningFile, (timeToWait, nPartition)...
        String systemConfigFile = args[0];
        String partitioningFile = args[1];
        ClientController clientController = new ClientController(partitioningFile, systemConfigFile);
        for (int i = 2; i < args.length; i+=2) {
            int delayInMs = Integer.parseInt(args[i]);
            int nPartition = Integer.parseInt(args[i + 1]);
            System.out.println("Sending command to oracle in " + delayInMs + "ms to partition in " + nPartition);
            Thread.sleep(delayInMs);
            Command command = new Command(nPartition);
            clientController.multicastQuery(command);
            System.out.println("Sent, partition: " + nPartition);
        }
    }
}

