/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.chirperv2.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chirperv2.util.Util;
import ch.usi.dslab.lel.dynastarv2.PartitionStateMachine;
import ch.usi.dslab.lel.dynastarv2.command.Command;
import ch.usi.dslab.lel.dynastarv2.messages.CommandType;
import ch.usi.dslab.lel.dynastarv2.messages.MessageType;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import ch.usi.dslab.lel.dynastarv2.probject.PRObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ChirperServer extends PartitionStateMachine {
    public static final Logger log = LoggerFactory.getLogger(ChirperServer.class);
    final String STATUS_OK = "OK";
    final String STATUS_RETRY = "RETRY";
    int creationSeq = 0;
    boolean running = true;
    Thread serverThread;

    public ChirperServer(int replicaId, String systemConfig, String partitionsConfig) {
        super(replicaId, systemConfig, partitionsConfig);
    }

    public static void main(String[] args) {
        log.debug("Entering main().");

        if (args.length != 4 && args.length != 10) {
            System.err.println("usage: replicaId systemConfigFile partitioningFile socialNetworkFile");
            System.err.println("usage: replicaId systemConfigFile partitioningFile socialNetworkFile preloadData gathererHost" +
                    "gathererPort monitorLogDir gathererDuration, gathererWarmup");
            System.exit(1);
        }

        if (args.length == 4) {
            int replicaId = Integer.parseInt(args[0]);
            String systemConfigFile = args[2];
            String partitionsConfigFile = args[3];
            String socialNetworkFile = args[4];
            ChirperServer chirperServer = new ChirperServer(replicaId, systemConfigFile, partitionsConfigFile);
            chirperServer.loadSocialNetworkData(socialNetworkFile);
            chirperServer.runStateMachine();
        } else {
            int argIndex = 0;
            int replicaId = Integer.parseInt(args[argIndex++]);
            String systemConfigFile = args[argIndex++];
            String partitionsConfigFile = args[argIndex++];
            String socialNetworkFile = args[argIndex++];
            boolean preloadData = Boolean.parseBoolean(args[argIndex++]);
            String gathererHost = args[argIndex++];
            int gathererPort = Integer.parseInt(args[argIndex++]);
            String gathererDir = args[argIndex++];
            int gathererDuration = Integer.parseInt(args[argIndex++]);
            int gathererWarmup = Integer.parseInt(args[argIndex++]);

            ChirperServer chirperServer = new ChirperServer(replicaId, systemConfigFile, partitionsConfigFile);
            if (preloadData) {
                chirperServer.loadSocialNetworkData(socialNetworkFile);
                System.out.println("DynaStar, Sample data loaded...");
            } else {
                System.out.println("Server skipping preload data");
            }
            chirperServer.setupMonitoring(gathererHost, gathererPort, gathererDir, gathererDuration, gathererWarmup);

            chirperServer.runStateMachine();
        }
    }

    @Override
    protected PRObject createObject(PRObject prObject) {
        log.debug("Creating user {}.", prObject.getId());
        User newUser = new User(prObject.getId());
        return newUser;
    }

    public void loadSocialNetworkData(String socialNetworkFilename) {
        Set<SocialNetworkGenerator.RandomUser> socialNetwork = Util.loadSocialNetwork(socialNetworkFilename);
        Map<ObjId, SocialNetworkGenerator.RandomUser> userCache = new HashMap<>();
        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            ObjId userId = User.genObjId(user.userId);
            PRObject prObject = createObject(userId, user.partitionId);
            indexObject(prObject, user.partitionId);
        }
        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            ObjId userId = User.genObjId(user.userId);
            userCache.put(userId, user);
            User chirperUser = (User) getObject(userId);
            Post post = new Post(-1L, userId, -1, 1416409000000L + userId.value, (userId.getStringValue()).getBytes());
            chirperUser.posts.addPost(post);

            for (int followerId : user.followers) {
                // conservative users
                ObjId objId = User.genObjId(followerId);
                chirperUser.followers.people.add(objId);
                User follower = (User) getObject(objId);
                follower.followeds.people.add(User.genObjId(user.userId));
                follower.materializedTimeline.addPost(post);
            }
        }

        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            if (user.partitionId != this.partitionId) {
                removeObject(User.genObjId(user.userId));
            }
        }
    }

    public void stop() {
        running = false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Message executeCommand(Command command) {
        logger.trace("cmd {} app server start executing command", command.getId(), command);
        MessageType genericType = (MessageType) command.getNext();
        AppCommandType cmdType = null;
        if (genericType instanceof AppCommandType)
            cmdType = (AppCommandType) genericType;
        else {
            CommandType tmp = (CommandType) genericType;
            switch (tmp) {
                case CONNECT:
                    cmdType = AppCommandType.FOLLOW;
                    break;
            }
        }
        Object obj = command.peekNext();
        ObjId userId = null;
        if (obj instanceof Set<?>) {
            HashSet<ObjId> objIds = (HashSet) command.getNext();
            if (objIds.size() > 0)
                userId = objIds.iterator().next();
        } else {
            userId = (ObjId) command.getNext();
        }


        Message reply = null;
        switch (cmdType) {
            case GETINFO:
//                System.out.print("deliver command " + command);
                reply = new Message("OK");
                break;
            case NEWUSER:
                break;
            case POST: {
//                 new Command(CommandType.POST_MATERIALIZE, userId, postFollowersList, post);
                command.rewind();
                cmdType = (AppCommandType) command.getNext();
//                TODO: reenable this for accepting follower list
                userId = (ObjId) command.getNext();
                Post post = (Post) command.getNext();
                Set<ObjId> followers = (Set<ObjId>) command.getNext();
//                System.out.print("User " + userId + " posting content " + post + " to follower:" + followers);
//                TODO: reenable this for accepting follower list
                reply = post(userId, followers, post);
                if (reply == null) {
                    log.error("cmd {} can't retrieve object {}", command.getId(), userId);
                    System.out.println("ERROR: cmd " + command.getId());
                    reply = new Message("RETRY");
                }
//                reply = post(userId, post);
                break;
            }

            case FOLLOW: {
                ObjId followerId = userId;
                Object test = command.getNext();
                ObjId followedId = null;
                if (test instanceof ObjId) {
                    followedId = (ObjId) test;
                } else if (test instanceof List) {
                    List<ObjId> followedIds = (List) test;
                    followedId = followedIds.get(1);
                }
                reply = follow(followerId, followedId);
                break;
            }

            case FOLLOW_NOP: {
                ObjId followerId = userId;
                List<ObjId> followedIds = (List) command.getNext();
                ObjId followedId = followedIds.get(0);

                reply = follow_nop(followerId, followedId);
                break;
            }

            case UNFOLLOW: {
                ObjId followerId = userId;
                ObjId followedId = (ObjId) command.getNext();

                reply = unfollow(followerId, followedId);
                break;
            }

            case UNFOLLOW_NOP: {
                ObjId followerId = userId;
                ObjId followedId = (ObjId) command.getNext();

                reply = unfollow_nop(followerId, followedId);
                break;
            }

            case GETTIMELINE: {
                reply = getTimeline(userId);
                break;
            }

            case GETFOLLOWEDLIST: {
                reply = getFollowedList(userId);
                break;
            }

            case GETFOLLOWERLIST: {
                reply = getFollowerList(userId);
                break;
            }

            case GETPOSTS: {
                reply = getPosts(userId);
                break;
            }

            default:
                break;
        }

        return reply;
    }

    private Message post(ObjId userId, Post post) {
        log.debug("User {} posting content: {}.", userId, post);

        User poster = (User) getObject(userId);
        UserList currentFollowers = poster.getFollowerList();
        log.debug("Follower list after calling intercepted method: {}", currentFollowers);
        Set<ObjId> currentFollowersIds = currentFollowers.getRawList();

        String status;
        // TODO: temp disable this for performance
        if (getMissingObjects(currentFollowersIds).size() == 0) {
            log.debug("Adding post {} to postlist of user {}", post.content, userId);
            poster.post(post);
            for (ObjId followerId : currentFollowersIds) {
                log.debug("Adding post {} to timeline of user {}", post.content, followerId);
                User follower = (User) getObject(followerId);
                if (follower != null)
                    follower.addToMaterializedTimeline(post);
            }
            status = "OK";
        } else {
            log.debug("Incomplete follower list. Please, retry.");
            status = "RETRY";
        }

        log.debug("post status: " + status);

        return new Message(status, new HashSet<>(), new HashSet<>());
    }


    Message post(ObjId userId, Set<ObjId> sentFollowers, Post post) {
        log.debug("User {} posting content: {}, followers {}", userId, post, sentFollowers);

        User poster = (User) getObject(userId);
        if (poster == null) {
            return null;
        }
        UserList currentFollowers = poster.getFollowerList();
        log.debug("Follower list after calling intercepted method: {}", currentFollowers);
        Set<ObjId> currentFollowersIds = currentFollowers.getRawList();

        String status;
        Set<ObjId> followersToAdd;
        Set<ObjId> followersToRemove;

        // TODO: temp disable this for performance
        if (getMissingObjects(currentFollowersIds).size() == 0) {
            log.debug("Adding post {} to postlist of user {}", post.content, userId);
            poster.post(post);
            for (ObjId followerId : currentFollowersIds) {
                log.debug("Adding post {} to timeline of user {}", post.content, followerId);
                User follower = (User) getObject(followerId);
                if (follower != null)
                    follower.addToMaterializedTimeline(post);
            }
            status = "OK";
        } else {
            log.debug("Incomplete follower list. Please, retry.");
            status = "RETRY";
        }

        followersToAdd = new HashSet<>(getMissingObjects(currentFollowersIds));
        followersToAdd.addAll(currentFollowersIds);
        followersToAdd.removeAll(sentFollowers);
//
        followersToRemove = new HashSet<>(sentFollowers);
        followersToRemove.removeAll(currentFollowersIds);

        log.debug("post status: " + status);

        return new Message(status, followersToAdd, followersToRemove);
    }

    Message follow(ObjId followerId, ObjId followedId) {
        log.debug("User {} now following user {}.", followerId, followedId);

        User followed = (User) getObject(followedId);
        User follower = (User) getObject(followerId);
        followed.addFollower(followerId);
        follower.follow(followedId);
        follower.addToMaterializedTimeline(followed.getUserPosts());

        return new Message("OK");
    }

    @SuppressWarnings("unused")
    Message follow_nop(ObjId followerId, ObjId followedId) {
        log.debug("User {} now following user {}.", followerId, followedId);

        User followed = (User) getObject(followedId);
        User follower = (User) getObject(followerId);
        followed.getUserPosts();

//      follower.
        return new Message("OK");
    }

    Message unfollow(ObjId followerId, ObjId followedId) {
        log.debug("User {} now unfollowing user {}.", followerId, followedId);

        User follower = (User) getObject(followerId);
        boolean didFollow = follower.unfollow(followedId);
        if (didFollow)
            follower.getMaterializedTimeline().removePostsFromUser(followedId);

        return new Message("OK");
    }

    @SuppressWarnings("unused")
    Message unfollow_nop(ObjId followerId, ObjId followedId) {
        log.debug("User {} now unfollowing user {}.", followerId, followedId);

        User follower = (User) getObject(followerId);
        return new Message("OK");
    }

    Message getTimeline(ObjId userId) {
        log.debug("Getting timeline for user {}.", userId);

        User user = (User) getObject(userId);
        List<Post> timeLine = user.getMaterializedTimeline().getRawList();

        return new Message(timeLine);
    }

    Message getFollowedList(ObjId userId) {
        log.debug("User {} requesting their list of followeds.", userId);

        return new Message(((User) getObject(userId)).getFollowedList().getRawList());
    }


    Message getFollowerList(ObjId userId) {
        log.debug("User {} requesting their list of followers.", userId);

        return new Message(((User) getObject(userId)).getFollowerList().getRawList());
    }

    Message getPosts(ObjId userId) {
        log.debug("User {} requesting their list of posts.", userId);

        return new Message(((User) getObject(userId)).getUserPosts().getRawList());
    }

    public PRObject createObject(ObjId id, Object value) {
        log.debug("Creating user {}.", id);
        User newUser = new User(id);
        return newUser;
    }

}
