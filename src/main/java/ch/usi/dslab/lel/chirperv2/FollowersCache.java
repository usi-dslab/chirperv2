package ch.usi.dslab.lel.chirperv2;


import ch.usi.dslab.lel.dynastarv2.probject.ObjId;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by longle on 26.04.17.
 */
public class FollowersCache {
    private static Map<ObjId, FollowersCache> allCaches = new ConcurrentHashMap<>();
    private Set<ObjId> followers = new HashSet<>();

    synchronized public static FollowersCache getCacheForUser(ObjId userId) {
        FollowersCache cache = allCaches.get(userId);
        if (cache == null) {
            cache = new FollowersCache();
            allCaches.put(userId, cache);
        }
        return cache;
    }

    synchronized public void setCache(Set<ObjId> followersList) {
        this.followers = new HashSet<>(followersList);
    }

    synchronized public void updateCache(Set<ObjId> followersToAdd, Set<ObjId> followersToRemove) {
        addFollowers(followersToAdd);
        removeFollowers(followersToRemove);
    }

    synchronized public void addFollowers(Set<ObjId> followersToAdd) {
        followers.addAll(followersToAdd);
    }

    synchronized public void removeFollowers(Set<ObjId> followersToRemove) {
        followers.removeAll(followersToRemove);
    }

    synchronized public Set<ObjId> getFollowersCache() {
        return new HashSet<>(followers);
    }
}
