/*

 chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.benchmarks;


import ch.usi.dslab.lel.chirperv2.ChirperClient;
import ch.usi.dslab.lel.chirperv2.Post;
import ch.usi.dslab.lel.chirperv2.User;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ChirperBenchClient implements ChirperClientInterface {
    ChirperClient chirperClient;


    public ChirperBenchClient(ChirperClient client) {
        chirperClient = client;
    }

    public int getUserFollowersCount(int userId) {
        return chirperClient.getUserFollowersCount(userId);
    }

    public void loadSocialNetworkIntoCache(String file) {
        chirperClient.loadSocialNetworkIntoCache(file);
    }

    public void updateFollowerCache(int userId, int followerToAdd, boolean isAdding) {
        chirperClient.updateFollowerCache(User.genObjId(userId), User.genObjId(followerToAdd), isAdding);
    }

    @Override
    public void createUser(int userId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chirperClient.createUserNB(User.genObjId(userId), tmpAccept);
    }

    @Override
    public void post(int userId, byte[] post, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context) {
        Post postObj = new Post(chirperClient.getId(), User.genObjId(userId), System.currentTimeMillis(), post);
        Consumer tmpAccept = o -> accept.accept(o, context);
        chirperClient.postNB(User.genObjId(userId), postObj, tmpAccept);
    }

    @Override
    public void follow(int followerId, int followedId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chirperClient.followNB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

    @Override
    public void unfollow(int followerId, int followedId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chirperClient.unfollowNB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

    @Override
    public void getTimeline(int userId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chirperClient.getTimelineNB(User.genObjId(userId), tmpAccept);
    }

}
