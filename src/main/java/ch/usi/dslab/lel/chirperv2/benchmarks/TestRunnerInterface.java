/*

 chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.benchmarks;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.monitors.LatencyDistributionPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.chirperv2.AppCommandType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;

public interface TestRunnerInterface {

    void releasePermit();

    class BenchCallbackContext {
        private static AtomicLong lastReqId = new AtomicLong();
        long startTimeNano;
        long timelineBegin;
        AppCommandType commandType;
        long reqId;
        public int nodeSize;
        CompletableFuture<Message> callback = null;


        public BenchCallbackContext(AppCommandType commandType) {
            this.startTimeNano = System.nanoTime();
            this.timelineBegin = System.currentTimeMillis();
            this.commandType = commandType;
            this.reqId = lastReqId.incrementAndGet();
        }

        public BenchCallbackContext(AppCommandType commandType, CompletableFuture callback) {
            this.startTimeNano = System.nanoTime();
            this.timelineBegin = System.currentTimeMillis();
            this.commandType = commandType;
            this.reqId = lastReqId.incrementAndGet();
            this.callback = callback;
        }

        public BenchCallbackContext(long startTimeNano, long timelineBegin, AppCommandType commandType) {
            this.startTimeNano = startTimeNano;
            this.timelineBegin = timelineBegin;
            this.commandType = commandType;
            this.reqId = lastReqId.incrementAndGet();
        }
    }

    class BenchCallbackHandlerWithContext implements BiConsumer {
        TestRunnerInterface parentRunner;

        private LatencyPassiveMonitor overallLatencyMonitor, postLatencyMonitor, followLatencyMonitor, unfollowLatencyMonitor, getTimelineLatencyMonitor;
        private ThroughputPassiveMonitor overallThroughputMonitor, postThroughputMonitor, followThroughputMonitor, unfollowThroughputMonitor, getTimelineThroughputMonitor;
        //        private TimelinePassiveMonitor overallTimelineMonitor, postTimelineMonitor, followTimelineMonitor, unfollowTimelineMonitor, getTimelineTimelineMonitor;
        private LatencyDistributionPassiveMonitor cdfMonitor;

        public BenchCallbackHandlerWithContext(TestRunnerInterface parentRunner, int clientId) {
            String client_mode = "client";
            this.parentRunner = parentRunner;

            // Latency monitors
            overallLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_overall");
            postLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_post", false);
            followLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_gettimeline", false);
            cdfMonitor = new LatencyDistributionPassiveMonitor(clientId, client_mode + "_overall");

            // Throughput monitors
            overallThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_overall");
            postThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_post", false);
            followThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_gettimeline", false);

            // Timeline monitor
//            overallTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_overall");
//            postTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_post", false);
//            followTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_follow", false);
//            unfollowTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_unfollow", false);
//            getTimelineTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_gettimeline", false);

        }


        @Override
        public void accept(Object reply, Object context) {
            parentRunner.releasePermit();
            Message replyMsg = (Message) reply;
            ((Message) reply).rewind();
            BenchCallbackContext benchContext = (BenchCallbackContext) context;
            long nowNano = System.nanoTime();
            long nowClock = System.currentTimeMillis();

            // log latency
            overallLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
//            overallTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
//                    "oracle_deliver", replyMsg.t_oracle_deliver,
//                    "oracle_dequeued", replyMsg.t_oracle_dequeued,
//                    "oracle_execute", replyMsg.t_oracle_execute,
//                    "oracle_execute_finish", replyMsg.t_oracle_finish_excecute,
//                    "partition_devlier", replyMsg.t_partition_deliver,
//                    "partition_dequeued", replyMsg.t_partition_dequeued,
//                    "partition_execute", replyMsg.t_partition_execute,
//                    "partition_execute_finish", replyMsg.t_partition_finish_excecute,
//                    "client_received", nowClock,
//                    "object_count", benchContext.nodeSize);
            cdfMonitor.logLatencyForDistribution(benchContext.startTimeNano, nowNano);

            // increment throughput count
            overallThroughputMonitor.incrementCount();

            LatencyPassiveMonitor requestLatencyMonitor = null;
//            TimelinePassiveMonitor requestTimelineMonitor = null;
            ThroughputPassiveMonitor requestThroughputMonitor = null;

            if (((Message) reply).peekNext().equals("OK") && benchContext.callback != null) {
                try {
                    benchContext.callback.complete((Message) reply);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            switch (benchContext.commandType) {
                case POST:
                case GETINFO:
                    requestLatencyMonitor = postLatencyMonitor;
//                    requestTimelineMonitor = postTimelineMonitor;
                    requestThroughputMonitor = postThroughputMonitor;
                    break;
                case FOLLOW:
                case FOLLOW_NOP:
                    requestLatencyMonitor = followLatencyMonitor;
//                    requestTimelineMonitor = followTimelineMonitor;
                    requestThroughputMonitor = followThroughputMonitor;
                    break;
                case UNFOLLOW:
                case UNFOLLOW_NOP:
                    requestLatencyMonitor = unfollowLatencyMonitor;
//                    requestTimelineMonitor = unfollowTimelineMonitor;
                    requestThroughputMonitor = unfollowThroughputMonitor;
                    break;
                case GETTIMELINE:
                    requestLatencyMonitor = getTimelineLatencyMonitor;
//                    requestTimelineMonitor = getTimelineTimelineMonitor;
                    requestThroughputMonitor = getTimelineThroughputMonitor;
                    break;
                default:
                    break;
            }
            if (!((Message) reply).peekNext().equals("DROPPED"))
                requestLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
//            if (nowNano - benchContext.startTimeNano > 200000000)
//                System.out.println("slow: " + reply + " - " + (nowNano - benchContext.startTimeNano) + " size " + benchContext.nodeSize);
//            requestTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
//                    "oracle_deliver", replyMsg.t_oracle_deliver,
//                    "oracle_dequeued", replyMsg.t_oracle_dequeued,
//                    "oracle_execute", replyMsg.t_oracle_execute,
//                    "oracle_execute_finish", replyMsg.t_oracle_finish_excecute,
//                    "partition_devlier", replyMsg.t_partition_deliver,
//                    "partition_dequeued", replyMsg.t_partition_dequeued,
//                    "partition_execute", replyMsg.t_partition_execute,
//                    "partition_execute_finish", replyMsg.t_partition_finish_excecute,
//                    "client_received", nowClock,
//                    "object_count", benchContext.nodeSize);
            if (!((Message) reply).peekNext().equals("DROPPED")) requestThroughputMonitor.incrementCount();
        }

    }


    class DynamicBenchCallbackHandlerWithContext implements BiConsumer {
        TestRunnerInterface parentRunner;

        private LatencyPassiveMonitor overallLatencyMonitor, postLatencyMonitor, followLatencyMonitor, unfollowLatencyMonitor, getTimelineLatencyMonitor;
        private ThroughputPassiveMonitor overallThroughputMonitor, postThroughputMonitor, followThroughputMonitor, unfollowThroughputMonitor, getTimelineThroughputMonitor;
        //        private TimelinePassiveMonitor overallTimelineMonitor, postTimelineMonitor, followTimelineMonitor, unfollowTimelineMonitor, getTimelineTimelineMonitor;
        private LatencyDistributionPassiveMonitor cdfMonitor;

        public DynamicBenchCallbackHandlerWithContext(TestRunnerInterface parentRunner, int clientId) {
            String client_mode = "client_conservative";
            this.parentRunner = parentRunner;

            // Latency monitors
            overallLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_overall");
            postLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_post", false);
            followLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_gettimeline", false);
            cdfMonitor = new LatencyDistributionPassiveMonitor(clientId, client_mode + "_overall");

            // Throughput monitors
            overallThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_overall");
            postThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_post", false);
            followThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_gettimeline", false);

            // Timeline monitor
//            overallTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_overall");
//            postTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_post", false);
//            followTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_follow", false);
//            unfollowTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_unfollow", false);
//            getTimelineTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_gettimeline", false);

        }


        @Override
        public void accept(Object reply, Object context) {
            parentRunner.releasePermit();
            Message replyMsg = (Message) reply;
            ((Message) reply).rewind();
            BenchCallbackContext benchContext = (BenchCallbackContext) context;
            long nowNano = System.nanoTime();

            LatencyPassiveMonitor requestLatencyMonitor = null;
            ThroughputPassiveMonitor requestThroughputMonitor = null;

            if ((((Message) reply).peekNext().equals("OK") || ((Message) reply).peekNext().equals("DROPPED"))
                    && benchContext.callback != null) {
                try {
                    benchContext.callback.complete((Message) reply);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            switch (benchContext.commandType) {
                case POST:
                case GETINFO:
                    requestLatencyMonitor = postLatencyMonitor;
//                    requestTimelineMonitor = postTimelineMonitor;
                    requestThroughputMonitor = postThroughputMonitor;
                    overallLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
                    cdfMonitor.logLatencyForDistribution(benchContext.startTimeNano, nowNano);
                    overallThroughputMonitor.incrementCount();
                    break;
                case FOLLOW:
                case FOLLOW_NOP:
                    requestLatencyMonitor = followLatencyMonitor;
//                    requestTimelineMonitor = followTimelineMonitor;
                    requestThroughputMonitor = followThroughputMonitor;
                    break;
                case UNFOLLOW:
                case UNFOLLOW_NOP:
                    requestLatencyMonitor = unfollowLatencyMonitor;
//                    requestTimelineMonitor = unfollowTimelineMonitor;
                    requestThroughputMonitor = unfollowThroughputMonitor;
                    break;
                case GETTIMELINE:
                    requestLatencyMonitor = getTimelineLatencyMonitor;
//                    requestTimelineMonitor = getTimelineTimelineMonitor;
                    requestThroughputMonitor = getTimelineThroughputMonitor;
                    break;
                default:
                    break;
            }
            if (!((Message) reply).peekNext().equals("DROPPED"))
                requestLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
//            if (nowNano - benchContext.startTimeNano > 200000000)
//                System.out.println("slow: " + reply + " - " + (nowNano - benchContext.startTimeNano) + " size " + benchContext.nodeSize);
//            requestTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
//                    "oracle_deliver", replyMsg.t_oracle_deliver,
//                    "oracle_dequeued", replyMsg.t_oracle_dequeued,
//                    "oracle_execute", replyMsg.t_oracle_execute,
//                    "oracle_execute_finish", replyMsg.t_oracle_finish_excecute,
//                    "partition_devlier", replyMsg.t_partition_deliver,
//                    "partition_dequeued", replyMsg.t_partition_dequeued,
//                    "partition_execute", replyMsg.t_partition_execute,
//                    "partition_execute_finish", replyMsg.t_partition_finish_excecute,
//                    "client_received", nowClock,
//                    "object_count", benchContext.nodeSize);
            if (!((Message) reply).peekNext().equals("DROPPED")) requestThroughputMonitor.incrementCount();
        }

    }
}

