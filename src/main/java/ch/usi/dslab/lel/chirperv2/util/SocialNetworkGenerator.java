/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.util;

import org.apache.commons.math3.distribution.ZipfDistribution;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class SocialNetworkGenerator {

    private static double skew = 1.2d;
    private static float ffRatio = 14f / 9f;
    private static int maxFollowers = 20;
    private static int numPartitions = 0;

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("usage: numUsers numPartitions filePath");
            System.exit(1);
        }
        int numUsers = Integer.parseInt(args[0]);
        numPartitions = Integer.parseInt(args[1]);
        String filePath = args[2];
        maxFollowers = (int) Math.round((Math.log(numUsers) / Math.log(100)) * 5);
        maxFollowers = 7;
        System.out.println("maxfollower: " + maxFollowers);
        SocialNetworkGenerator rng = new SocialNetworkGenerator();
        List<RandomUser> network = rng.createNetwork(numUsers, numPartitions);
        rng.saveNetwork(network, filePath);
    }

    public List<RandomUser> createNetwork(int numUsers, int numPartitions) {
        System.out.println("Calling getFollowerInSameOrNeighborPartition");
        RandomNumberGenerator rand = RandomNumberGenerator.getInstance();
        Map<Integer, RandomPartition> allPartitions = new HashMap<>();
        List<RandomUser> allUsers = new ArrayList<>();
        for (int i = 0; i < numPartitions; i++) {
            allPartitions.put(i + 1, new RandomPartition(i + 1));
        }
        for (int i = 0; i < numUsers; i++) {
            RandomUser user = new RandomUser(i);
            allUsers.add(user);
            user.numFollowers = user.availableFollowerSlots = rand.getZipfRandomInt(1, maxFollowers, skew);
            user.numFolloweds = user.availableFollowedSlots = correlateFollowedsFromFollowers(user.numFollowers);
            user.partitionId = 1 + user.interest % numPartitions;
            RandomPartition partition = allPartitions.get(user.partitionId);
            partition.users.add(user);
        }
        for (int i = 0; i < numUsers; i++) {
            RandomUser user = allUsers.get(i);
            addFollowerWithSameInterestAcrossPartitions(user, allUsers, allPartitions);
//            addFollowerWithExactSameInterestAcrossPartitions(user, allUsers, allPartitions);
        }

//        for (int i = 0; i < numUsers; i++) {
//            RandomUser user = allUsers.get(i);
//            while (user.availableFollowerSlots > 0) {
////                RandomUser follower = getFollowerInSameOrNeighborPartition(user, allPartitions);
//                RandomUser follower = getFollowerAcrossPartitions(user, allPartitions);
//                follower.availableFollowedSlots--;
//                follower.followeds.add(user.userId);
//                user.followers.add(follower.userId);
//                user.availableFollowerSlots--;
//            }
//        }
        return allUsers;
    }

    @SuppressWarnings("unchecked")
    public void saveNetwork(List<RandomUser> allUsers, String filePath) {
        for (RandomUser user : allUsers) {
//            System.out.println("User " + user.userId + " (partition " + user.partitionId + " interest " + user.interest + "):");
//            System.out.println("\t" + user.followers.size() + " followers:" +
//                    user.followers.stream().map(u1 -> allUsers.stream().filter(u -> u.userId == u1).findFirst().get()).collect(Collectors.toList()));
//
//            System.out.println("\t" + user.followeds.size() + " friends");
        }
        JSONObject j_network = new JSONObject();
        j_network.put("numUsers", allUsers.size());
        JSONArray j_usersList = new JSONArray();
        j_network.put("allUsers", j_usersList);
        for (RandomUser user : allUsers) {
            JSONObject j_user = new JSONObject();
            j_usersList.add(j_user);
            j_user.put("userId", user.userId);
            j_user.put("partitionId", user.partitionId);
            j_user.put("interest", user.interest);
            JSONArray j_followersList = new JSONArray();
            j_user.put("followers", j_followersList);
            j_followersList.addAll(user.followers.stream().collect(Collectors.toList()));
        }

        try {
            FileWriter file = new FileWriter(filePath);
            file.write(j_network.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int correlateFollowedsFromFollowers(int numFollowers) {
        return Math.round(ffRatio * numFollowers);
    }

    /*public Map<Integer, RandomUser> getSameInterestUser(RandomUser user, Map<Integer, RandomUser> allUsers, Map<Integer, RandomPartition> allPartitions) {
        return allUsers.entrySet().stream()
                .filter(entrySet -> (entrySet.getValue().interest % allPartitions.maxSize() == user.interest % allPartitions.maxSize()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }*/

    public RandomUser getFollowerInSameOrNeighborPartition(RandomUser user, Map<Integer, RandomPartition> allPartitions) {
        RandomNumberGenerator rand = RandomNumberGenerator.getInstance();

        int followerPartitionId = Util.leastOdd(user.partitionId) + rand.getUniformRandomInt(0, 1);
        followerPartitionId = Util.clamp(followerPartitionId, 1, allPartitions.size());
        RandomPartition followerPartition = allPartitions.get(followerPartitionId);

        int round = 0;
        while (round < Integer.MAX_VALUE) {
            for (RandomUser candidateToFollower : followerPartition.users) {
                int extraSlots = candidateToFollower.numFolloweds * round;
                if (candidateToFollower.userId != user.userId
                        && (candidateToFollower.availableFollowedSlots + extraSlots) > 0
                        && !user.followers.contains(candidateToFollower.userId)) {
                    return candidateToFollower;
                }
            }
            round++;
        }
        return null;
    }

    public void addFollowerWithSameInterestAcrossPartitions(RandomUser user, List<RandomUser> allUsers, Map<Integer, RandomPartition> allPartitions) {
        Map<Integer, Integer> interestMap = generateFollowerRate(user, allPartitions.size());
        Map<Integer, Integer> followerCount = new HashMap<>();
        List<RandomUser> ret = new ArrayList<>();
        int round = 0;
        while (user.availableFollowerSlots > 0 && round < Integer.MAX_VALUE) {
            for (RandomUser candidate : allUsers) {
                int interestIndex = candidate.interest % allPartitions.size();
                int extraSlots = candidate.numFolloweds * round;
                if (followerCount.get(interestIndex) == null) followerCount.put(interestIndex, 0);
                if (interestMap.get(interestIndex) == null) interestMap.put(interestIndex, 0);
                if (followerCount.get(interestIndex) < interestMap.get(interestIndex)
                        && candidate.userId != user.userId
//                        && user.interest % allPartitions.size() == interestIndex
                        && (candidate.availableFollowedSlots + extraSlots) > 0) {
//                    ret.add(candidate);
                    candidate.availableFollowedSlots--;
                    candidate.followeds.add(user.userId);
                    user.followers.add(candidate.userId);
                    user.availableFollowerSlots--;
                    followerCount.put(interestIndex, followerCount.get(interestIndex) + 1);
                }
                round++;
            }
////                RandomUser follower = getFollowerInSameOrNeighborPartition(user, allPartitions);
//            RandomUser follower = getFollowerWithSameInterestAcrossPartitions(user, allUsers, allPartitions);
//            follower.availableFollowedSlots--;
//            follower.followeds.add(user.userId);
//            user.followers.add(follower.userId);
//            user.availableFollowerSlots--;
        }

//        System.out.println(user.toString() + interestMap);
    }

    public Map<Integer, Integer> generateFollowerRate(RandomUser user, int interestCount) {
        RandomNumberGenerator rand = RandomNumberGenerator.getInstance();
        Map<Integer, Integer> rate = new HashMap<>();
        for (int i = 0; i < user.availableFollowerSlots; i++) {
            int j = rand.getZipfRandomInt(1, interestCount, skew);
            if (rate.get(j) == null) {
                rate.put(j, 1);
            } else {
                rate.put(j, rate.get(j) + 1);
            }
        }
        Integer v = rate.get(1);
        Integer tmp = rate.get(user.interest % interestCount);
        if (tmp == null) tmp = 0;
        rate.put(1, tmp);
        rate.put(user.interest % interestCount, v);
        return rate;
    }

    public void addFollowerWithExactSameInterestAcrossPartitions(RandomUser user, List<RandomUser> allUsers, Map<Integer, RandomPartition> allPartitions) {
        Map<Integer, Integer> interestMap = new HashMap<>();
        interestMap.put(user.interest % allPartitions.size(), user.availableFollowerSlots);
        Map<Integer, Integer> followerCount = new HashMap<>();
        List<RandomUser> ret = new ArrayList<>();
        int round = 0;
        while (user.availableFollowerSlots > 0 && round < Integer.MAX_VALUE) {
            for (RandomUser candidate : allUsers) {
                int interestIndex = candidate.interest % allPartitions.size();
                int extraSlots = candidate.numFolloweds * round;
                if (followerCount.get(interestIndex) == null) followerCount.put(interestIndex, 0);
                if (interestMap.get(interestIndex) == null) interestMap.put(interestIndex, 0);
                if (followerCount.get(interestIndex) < interestMap.get(interestIndex)
                        && candidate.userId != user.userId
                        && (candidate.availableFollowedSlots + extraSlots) > 0) {
                    candidate.availableFollowedSlots--;
                    candidate.followeds.add(user.userId);
                    user.followers.add(candidate.userId);
                    user.availableFollowerSlots--;
                    followerCount.put(interestIndex, followerCount.get(interestIndex) + 1);
                }
                round++;
            }
        }
    }

    public List generateInterestList(RandomUser user, List<RandomUser> allUsers, Map<Integer, RandomPartition> allPartitions) {
        List<RandomUser> sameInterest = getSameInterestUser(user, allUsers, allPartitions);
        List<RandomUser> differentInterest = allUsers.stream().filter(u -> !sameInterest.contains(u)).collect(Collectors.toList());
        differentInterest.addAll(sameInterest);
        return differentInterest;
    }

    public List<RandomUser> getSameInterestUser(RandomUser user, List<RandomUser> allUsers, Map<Integer, RandomPartition> allPartitions) {
        return allUsers.stream()
                .filter(candidate ->
                        (candidate.interest % allPartitions.size() == user.interest % allPartitions.size()))
//                .peek(System.out::println)
                .collect(Collectors.toList());
    }

    public RandomUser getFollowerAcrossPartitions(RandomUser user, Map<Integer, RandomPartition> allPartitions) {
        RandomNumberGenerator rand = RandomNumberGenerator.getInstance();
        int followerPartitionId = rand.getUniformRandomInt(1, allPartitions.size());
        RandomPartition followerPartition = allPartitions.get(followerPartitionId);

        int round = 0;
        while (round < Integer.MAX_VALUE) {
            for (RandomUser candidateToFollower : followerPartition.users) {
                int extraSlots = candidateToFollower.numFolloweds * round;
                if (candidateToFollower.userId != user.userId
                        && (candidateToFollower.availableFollowedSlots + extraSlots) > 0
                        && !user.followers.contains(candidateToFollower.userId)) {
                    return candidateToFollower;
                }
            }
            round++;
        }
        return null;
    }

    public static class RandomNumberGenerator {
        static RandomNumberGenerator instance;
        Map<Double, double[]> zipfSkewAccTable = new Hashtable<Double, double[]>();
        Random uniformRandom = new Random(0);
        int tableSize = 100;
        public static RandomNumberGenerator getInstance() {
            if (instance == null)
                instance = new RandomNumberGenerator(100);
            return instance;
        }
        public static RandomNumberGenerator getInstance(int tableSize) {
            if (instance == null)
                instance = new RandomNumberGenerator(tableSize);
            return instance;
        }

        private RandomNumberGenerator(int tableSize){
            this.tableSize = tableSize;
        }

        public RandomNumberGenerator(){
            this.tableSize = 100;
        }

        public void changeSeed(long seed) {
            uniformRandom.setSeed(seed);
        }

        private double getZipfAcc(int x, double skew) {
            generateZipfCdfTable(skew);
            double ret = zipfSkewAccTable.get(skew)[x];
            return ret;
        }

        private void generateZipfCdfTable(double skew) {
            if (zipfSkewAccTable.containsKey(skew))
                return;

            double[] table = new double[tableSize];
            ZipfDistribution zipf = new ZipfDistribution(tableSize, skew);

            for (int i = 0; i < tableSize-1; i++)
                table[i] = zipf.cumulativeProbability(i + 1);
            table[tableSize-1] = 1.0d;

            zipfSkewAccTable.put(skew, table);
        }

        public int getZipfRandomInt(int max, double skew) {
            return getZipfRandomInt(0, max, skew);
        }

        public int getZipfRandomInt(int min, int max, double skew) {
            int range = max - min;
            double randomNumber = uniformRandom.nextDouble();

            double[] cdfTable = getZipfCdfTable(skew);

            double fraction = 0.0d;
            for (int i = 0; i < tableSize; i++) {
                if (randomNumber < cdfTable[i]) {
                    fraction = ((double) i) / tableSize;
                    break;
                }
            }

            int randomReturn = min + ((int) (fraction * ((double) range)));

            assert (min <= randomReturn && randomReturn <= max);

            return randomReturn;
        }

        private double[] getZipfCdfTable(double skew) {
            generateZipfCdfTable(skew);
            return zipfSkewAccTable.get(skew);
        }

        public int getUniformRandomInt(int min, int max) {
            return min + uniformRandom.nextInt(1 + max - min);
        }
    }

    public static class RandomUser {
        public int userId;
        public int interest;
        public int partitionId;
        public int numFollowers, availableFollowerSlots;
        public int numFolloweds, availableFollowedSlots;
        public List<Integer> followers, followeds;
//        public Set<Integer> followers, followeds;
        RandomNumberGenerator rand = RandomNumberGenerator.getInstance();

        public RandomUser(int id) {
            this();
            userId = id;
        }

        public RandomUser() {
//            followers = new HashSet<>();
//            followeds = new HashSet<>();
            followers = Collections.synchronizedList(new ArrayList<>());
            followeds = new ArrayList<>();
            interest = rand.getUniformRandomInt(1, 64);
        }

        public String toString() {
            return String.format("[u %d, i %d]", userId, interest);
        }
    }

    public static class RandomPartition {
        int partitionId;
        List<RandomUser> users;

        public RandomPartition(int id) {
            partitionId = id;
            users = new ArrayList<>();
        }
    }

}
