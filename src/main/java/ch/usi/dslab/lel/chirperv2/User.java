/*

 chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import ch.usi.dslab.lel.dynastarv2.probject.PRObject;

public class User extends PRObject {
    private static final long serialVersionUID = 1L;
    UserList followeds;
    UserList followers;
    PostsList posts;
    PostsList materializedTimeline;

    public User() {
        // default constructor for Kryo
    }

    public User(ObjId userId) {
        followeds = new UserList();
        followers = new UserList();
        posts = new PostsList();
        materializedTimeline = new PostsList();
        this.setId(userId);
    }


    public static ObjId genObjId(int userId) {
        return new ObjId(userId);
    }


    public static ObjId genObjId(String userId) {
        return new ObjId(Integer.parseInt(userId));
    }

    public void post(Post post) {
        posts.addPost(post);
    }

    public PostsList getUserPosts() {
        return posts;
    }

    public boolean follow(ObjId followedId) {
        return followeds.add(followedId);
    }

    public boolean unfollow(ObjId followedId) {
        return followeds.remove(followedId);
    }

    public boolean addFollower(ObjId followerId) {
        return followers.add(followerId);
    }

    public void removeFollower(ObjId followerId) {
        followers.remove(followerId);
    }

    public UserList getFollowedList() {
        return followeds;
    }

    public UserList getFollowerList() {
        return followers;
    }

    public void addToMaterializedTimeline(Post post) {
        if (followeds.contains(post.getPosterId()))
            materializedTimeline.addPost(post);
    }

    public void addToMaterializedTimeline(PostsList posts) {
        posts.postsList.stream().filter(post -> followeds.contains(post.getPosterId())).forEach(materializedTimeline::addPost);
    }

    public PostsList getMaterializedTimeline() {
        return materializedTimeline;
    }


    @Override
    public void updateFromDiff(Message objectDiff) {
        objectDiff.rewind();
        objectDiff.getNext(); // objId
        boolean isModified = (boolean) objectDiff.getNext();
        if (isModified) {
            PostsList postsList = (PostsList) objectDiff.getNext();
            if (postsList != null) this.materializedTimeline = postsList;
            this.followers = (UserList) objectDiff.getNext();
            this.followeds = (UserList) objectDiff.getNext();
        }

    }

    public Message getSuperDiff() {
        return new Message(this.getId(), this.materializedTimeline, this.followers, this.followeds);
    }


    @Override
    public String toString() {
        return "(user " + getId() + ")";
    }
}
