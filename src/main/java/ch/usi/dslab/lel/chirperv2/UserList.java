/*

 chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;


import ch.usi.dslab.lel.dynastarv2.probject.ObjId;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


public class UserList implements Serializable {
    Set<ObjId> people = new HashSet<>();;

    public UserList() {
    }

    public boolean add(User followedUser) {
        return add(followedUser.getId());
    }

    public boolean add(ObjId userId) {
        return people.add(userId);
    }

    public boolean remove(User user) {
        return remove(user.getId());
    }

    public boolean remove(ObjId userId) {
        return people.remove(userId);
    }

    public Set<ObjId> getRawList() {
        return people;
    }

    public boolean contains(ObjId userId) {
        return people.contains(userId);
    }

    @Override
    public String toString() {
        String sb = "";
        for (ObjId person : people)
            sb += person + " ";
        return sb;
    }

}
