/*

 chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chirper.
 
 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2.benchmarks;

import java.util.function.BiConsumer;

public interface ChirperClientInterface {

    default int getUserFollowersCount(int userId) {
        return 0;
    }

    void createUser(int userId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context);

    void post(int userId, byte[] post, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context);

    void follow(int followerId, int followedId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context);

    void unfollow(int followerId, int followedId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context);

    void getTimeline(int userId, BiConsumer accept, TestRunnerInterface.BenchCallbackContext context);

}
