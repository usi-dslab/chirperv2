/*

 chirper - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano

 This file is part of chirper.

 chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chirperv2;


import ch.usi.dslab.lel.chirperv2.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chirperv2.util.Util;
import ch.usi.dslab.lel.dynastarv2.OracleStateMachine;
import ch.usi.dslab.lel.dynastarv2.probject.ObjId;
import ch.usi.dslab.lel.dynastarv2.probject.PRObject;
import ch.usi.dslab.lel.dynastarv2.probject.PRObjectNode;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ChirperOracle extends OracleStateMachine {

    public ChirperOracle(int replicaId, String systemConfig, String partitionsConfig) {
        super(replicaId, systemConfig, partitionsConfig);
    }

    public static void main(String args[]) {
        String systemConfigFile;
        String partitionConfigFile;
        String socialNetworkFile;
        int oracleId;
        if (args.length == 10) {
            int argIndex = 0;
            oracleId = Integer.parseInt(args[argIndex++]);
            systemConfigFile = args[argIndex++];
            partitionConfigFile = args[argIndex++];
            socialNetworkFile = args[argIndex++];
            boolean preloadData = Boolean.parseBoolean(args[argIndex++]);

            String gathererHost = args[argIndex++];
            int gathererPort = Integer.parseInt(args[argIndex++]);
            String gathererDir = args[argIndex++];
            int gathererDuration = Integer.parseInt(args[argIndex++]);
            int gathererWarmup = Integer.parseInt(args[argIndex++]);

            ChirperOracle oracle = new ChirperOracle(oracleId, systemConfigFile, partitionConfigFile);
            if (preloadData) {
                oracle.loadSocialNetworkIntoCache(socialNetworkFile);
            } else {
                System.out.println("Oracle skipping preload data");
            }
            oracle.setupMonitoring(gathererHost, gathererPort, gathererDir, gathererDuration, gathererWarmup);
            oracle.runStateMachine();
        } else {
            System.out.print("Usage: <oracleId> <partitionId> <system config> <partition config>");
            System.exit(0);
        }
    }


    public void loadSocialNetworkIntoCache(String file) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(file);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {
            ObjId userId = User.genObjId(user.userId);
            PRObjectNode node = new PRObjectNode(userId, user.partitionId);
            this.objectGraph.addNode(node);
        }
    }


    @Override
    public PRObject createObject(ObjId id, Object value) {
        return null;
    }
}
