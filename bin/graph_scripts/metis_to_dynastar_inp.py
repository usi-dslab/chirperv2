from sets import Set

metis_out = 'Slashdot0811.metisinp.part.2'
metis_out = 'soc-Slashdot0811.txt.metis.part.2'
metis_inp = 'Slashdot0811.metisinp'
metis_inp = 'soc-Slashdot0811.txt.txt'

users_count = 77360
connections_count = 905468
dynastar_inp_file = 'users_' + str(77360) + '_partitions_2.txt.metis'

vertexies = []
partitions = []
graph = [Set() for i in range(users_count)]
with open(metis_inp) as f:
    next(f)
    for line in f:
        vertexies.append(line.rstrip('\n'))

with open(metis_out) as f:
    for line in f:
        partitions.append(line.rstrip('\n'))

dyn_out = open(dynastar_inp_file, 'w')
dyn_out.write("{}\n".format(users_count))
for i in range(0,len(vertexies)):
    dyn_out.write("{} {}\n".format(int(partitions[i])+1, vertexies[i][2:]))
dyn_out.flush()
dyn_out.close()
