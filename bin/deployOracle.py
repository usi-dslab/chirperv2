#!/usr/bin/python

import json
import logging
import sys

import common

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) != 10:
    print "usage: " + common.sarg(
        0) + " <config_file> <partitioning_file> <socialNetwork_file> <preloadData> <gatherer_host> <gatherer_port> <gatherer_dir> <gatherer_duration> <gatherer_Warmup>"
    sys.exit(1)


# ./deployOracle.py /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/systemConfigs/minimal_system_config.json /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/systemConfigs/minimal_partitioning.json /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/graphs/holme-kim/0.01/users_10000_partitions_2.json True localhost 50000 /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/logs 90 20000
config_file = common.sarg(1)
partitioning_file = common.sarg(2)
socialNetwork_file = common.sarg(3)
preloadData = common.sarg(4)
gatherer_host = common.sarg(5)
gatherer_port = common.iarg(6)
gatherer_dir = common.sarg(7)
gatherer_duration = common.iarg(8)
gatherer_warmup = common.iarg(9)
config_stream = open(config_file)
partition_stream = open(partitioning_file)
config = json.load(config_stream)
partition = json.load(partition_stream)


def getHostType(host):
    for group in partition["partitions"]:
        if host["pid"] in group["servers"]:
            return group["type"]


cmdList = []

for member in config["group_members"]:
    pid = member["pid"]
    group = member["group"]
    host = member["host"]
    port = member["port"]
    if getHostType(member) == "ORACLE":
        oracleCmdPieces = [common.getJavaExec(host, 'SERVER'), '-DHOSTNAME=' + str(pid) + "-" + str(group),
                           common.JAVA_CLASSPATH, common.CHIRPER_CLASS_ORACLE]
        oracleCmdPieces += [pid, config_file, partitioning_file, socialNetwork_file, preloadData]
        oracleCmdPieces += [gatherer_host, gatherer_port, gatherer_dir, gatherer_duration, gatherer_warmup]
        oracleCmdString = " ".join([str(val) for val in oracleCmdPieces])

        cmdList.append({"node": host, "port": port, "cmdstring": oracleCmdString})

config_stream.close()
partition_stream.close()

launcherThreads = []
# print cmdList

thread = common.LauncherThread(cmdList)
thread.start()
thread.join()
