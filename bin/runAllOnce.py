#!/usr/bin/python

import datetime
import sys
import time
from os.path import abspath

import common
import systemConfigurer
from common import farg
from common import iarg
from common import sarg, localcmdbg


# usage
def usage():
    print "usage: " \
          + sarg(0) \
          + " numClients numPartitions socialNetworkFile " \
            " preloadData [wp wf wu wtl name edgecut]"
    sys.exit(1)


# ./runAllOnce.py 1 2 /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/graphs/holme-kim/0.01/users_10000_partitions_2.json True 0 0 0 1 holme-kim 0.1
# parameters
numClients = iarg(1)
numPartitions = iarg(2)
socialNetworkFile = abspath(sarg(3))
preloadData = True if sarg(4).lower() == 'true' else False
clientNodes = None
numServers = 0
numCoordinators = 0
numAcceptors = 0
numOracles = 1

weightPost = farg(5)
weightFollow = farg(6)
weightUnfollow = farg(7)
weightGetTimeline = farg(8)
workloadName = sarg(9)
edge_cut = sarg(10)

if not common.LOCALHOST:
    common.localcmd(common.cleaner)
    time.sleep(5)

retwisServerNode = None
chirperSysConfig = None
serverList = None
oracleList = None
chirperSysConfigFile = None
chirperPartitioningFile = None
gathererNode = None
minClientId = None

# common.localcmd(common.systemParamSetter)


# disable clock sync
if not common.LOCALHOST:
    for node in common.getNonScreenNodes():
        common.sshcmdbg(node, common.continousClockSynchronizer)

chirperSysConfig = systemConfigurer.generateSystemConfiguration(numPartitions)
time.sleep(5)

serverList = chirperSysConfig["server_list"]
oracleList = chirperSysConfig["oracle_list"]
numServers = len(serverList)

chirperSysConfigFile = chirperSysConfig["config_file"]
chirperPartitioningFile = chirperSysConfig["partitioning_file"]
gathererNode = chirperSysConfig["gatherer_node"]
minClientId = chirperSysConfig["client_initial_pid"]
clientNodes = chirperSysConfig["remaining_nodes"]

# logfolder
logFolder = ""
logFolder = "_".join([str(val) for val in
                      [workloadName, edge_cut, "DynaStarV2", "p", numPartitions, "c", numClients,
                       "p", weightPost, "f", weightFollow, "uf",
                       weightUnfollow, "tl", weightGetTimeline, "#",
                       datetime.datetime.now().isoformat().replace(':', '-')]])

###########################################################################
# LAUNCH SERVERS
###########################################################################

# launch server(s)

# launch multicast infrastructure
mcastCmdPieces = [common.multicastDeployer, chirperSysConfigFile]
mcastCmdString = " ".join(mcastCmdPieces)
localcmdbg(mcastCmdString)

time.sleep(5)

# # launch replicas
chirperServerCmdPieces = [common.chirperServerDeployer, chirperSysConfigFile, chirperPartitioningFile,
                          socialNetworkFile, str(preloadData), gathererNode, str(common.gathererPort),
                          common.CHIRPER_LOG_SERVERS,
                          str(common.EXPERIMENT_DURATION), str(common.EXPERIMENT_WARMUP_MS)]
chirperServerCmdStr = " ".join(chirperServerCmdPieces)
localcmdbg(chirperServerCmdStr)

# time.sleep(5)
#
# launch oracle

chirperOracleCmdPieces = [common.chirperOracleDeployer, chirperSysConfigFile, chirperPartitioningFile,
                          socialNetworkFile, str(preloadData), gathererNode, str(common.gathererPort),
                          common.CHIRPER_LOG_SERVERS,
                          str(common.EXPERIMENT_DURATION), str(common.EXPERIMENT_WARMUP_MS)]
chirperClientCmdString = " ".join(chirperOracleCmdPieces)
localcmdbg(chirperClientCmdString)

print "Waiting for servers to be ready..."
time.sleep(5)
# time.sleep(3 * numPartitions + 5)
print "Servers should be ready."

if "456626" in socialNetworkFile:
    print "Wait for partitioning."
    time.sleep(90)
elif "81306" in socialNetworkFile or "100000" in socialNetworkFile:
    print "Wait for partitioning."
    time.sleep(10)

###########################################################################
# LAUNCH CLIENTS
###########################################################################

# launch clients
# deployTestRunners.py: socialNetworkFile numClients weightPost weightFollow weightUnfollow weightGetTimeline algorithm [(for chirper:) numPartitions minClientId configFile partsFile]"

print 'Deploying clients'
if not preloadData:
    chirperClientCmdPieces = [common.chirperClientDynamicDeployer, chirperSysConfigFile, chirperPartitioningFile,
                              numPartitions, numClients, minClientId, socialNetworkFile, weightPost, weightFollow,
                              weightUnfollow, weightGetTimeline]
else:
    chirperClientCmdPieces = [common.chirperClientDeployer, chirperSysConfigFile, chirperPartitioningFile,
                              numPartitions, numClients, minClientId, socialNetworkFile, weightPost, weightFollow,
                              weightUnfollow, weightGetTimeline]

chirperClientCmdString = " ".join([str(val) for val in chirperClientCmdPieces])
common.localcmd(chirperClientCmdString)
# print 'Deploying clients'
# chirperClientCmdPieces = [common.clientActorDeployer, socialNetworkFile, numClients, weightPost, weightFollow,
#                           weightUnfollow, weightGetTimeline, algorithm, uniformRandom, runningMode,
#                           numPartitions, minClientId, chirperSysConfigFile, chirperPartitioningFile]
# chirperClientCmdString = " ".join([str(val) for val in chirperClientCmdPieces])
# common.localcmd(chirperClientCmdString)



###########################################################################
# LAUNCH ACTIVE MONITORS
###########################################################################

if not common.LOCALHOST:
    for nonClient in serverList:
        # bwmCmdPieces = [common.javabin, common.javacp, common.javaBWMonitorClass, "PARTITION",
        #                 nonClient["pid"], chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                 common.SENSE_DURATION, common.nonclilogdirchirper]
        # bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        # common.sshcmdbg(nonClient["host"], bwmCmdString)

        cpumCmdPieces = [common.getJavaExec(chirperSysConfig["gatherer_node"], 'GATHERER'), common.JAVA_CLASSPATH,
                         common.javaCPUMonitorClass, "PARTITION", nonClient["pid"], chirperSysConfig["gatherer_node"],
                         str(common.gathererPort),
                         str(common.EXPERIMENT_DURATION), common.CHIRPER_LOG_SERVERS]
        cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        common.sshcmdbg(nonClient["host"], cpumCmdString)

        # memmCmdPieces = [common.javabin, common.javacp, common.javaMemoryMonitorClass, "PARTITION",
        #                  nonClient["pid"], chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                  common.SENSE_DURATION, common.nonclilogdirchirper]
        # memmCmdString = " ".join([str(val) for val in memmCmdPieces])
        # common.sshcmdbg(nonClient["host"], memmCmdString)

    for nonClient in oracleList:
        # bwmCmdPieces = [common.javabin, common.javacp, common.javaBWMonitorClass, "ORACLE",
        #                 nonClient["pid"], chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                 common.SENSE_DURATION, common.nonclilogdirchirper]
        # bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        # common.sshcmdbg(nonClient["host"], bwmCmdString)

        cpumCmdPieces = [common.getJavaExec(chirperSysConfig["gatherer_node"], 'GATHERER'), common.JAVA_CLASSPATH,
                         common.javaCPUMonitorClass, "ORACLE", nonClient["pid"], chirperSysConfig["gatherer_node"],
                         str(common.gathererPort),
                         str(common.EXPERIMENT_DURATION), common.CHIRPER_LOG_SERVERS]
        cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        common.sshcmdbg(nonClient["host"], cpumCmdString)


        # memmCmdPieces = [common.javabin, common.javacp, common.javaMemoryMonitorClass, "ORACLE",
        #                  nonClient["pid"], chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                  common.SENSE_DURATION, common.nonclilogdirchirper]
        # memmCmdString = " ".join([str(val) for val in memmCmdPieces])
        # common.sshcmdbg(nonClient["host"], memmCmdString)

        # client bw and cpu monitors
        # clientNodeMap = common.mapClientsToNodes(numClients, clientNodes)
        # cliNodeId = minClientId
        # for node in clientNodes:
        #     if common.clientNodeIsEmpty(node, clientNodeMap):
        #         continue
        #     bwmCmdPieces = [common.javabin, common.javacp, common.javaBWMonitorClass, "client_node",
        #                     cliNodeId, chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                     common.SENSE_DURATION,
        #                     common.clilogdirchirper]
        #     bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        #     common.sshcmdbg(node, bwmCmdString)
        #     cpumCmdPieces = [common.javabin, common.javacp, common.javaCPUMonitorClass, "client_node",
        #                      cliNodeId, chirperSysConfig["gatherer_node"], str(common.gathererPort),
        #                      common.SENSE_DURATION,
        #                      common.clilogdirchirper]
        #     cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        #     common.sshcmdbg(node, cpumCmdString)
        #     cliNodeId += 1

###########################################################################
# LAUNCH GATHERER
###########################################################################

# launch gatherer last and wait for it to finish
# wait for gatherer with timeout; if timeout, kill everything, including Redis
# gathererCmdPieces = [common.gathererDeployer, gathererNode, algorithm, numPartitions, numClients, numServers,
#                      numCoordinators, numAcceptors, logFolder]

common.localcmd("mkdir -p " + common.CHIRPER_LOG_SERVERS)
fullLogDir = common.CHIRPER_LOG_BASE + logFolder + "/"
detailLogDir = common.CHIRPER_LOG_BASE + logFolder + "/details/"
gathererCmdPieces = [common.gathererDeployer, gathererNode, numPartitions, numClients, numServers,
                     numOracles, detailLogDir]

gathererCmd = " ".join([str(val) for val in gathererCmdPieces])

#
# print gathererCmd
common.localcmd(gathererCmd)

###########################################################################
# processing logs
###########################################################################
# collect main log files
common.localcmd("cp " + common.benchCommonPath + " " + detailLogDir)
common.localcmd("cp " + common.runBatchPath + " " + detailLogDir)
common.localcmd("cp " + common.SYSTEM_CONFIG_FILE + " " + detailLogDir)
common.localcmd("cp " + common.PARTITION_CONFIG_FILE + " " + detailLogDir)
common.localcmdbg("mv " + common.CHIRPER_LOG_SERVERS + " " + detailLogDir)

main_logs = ["throughput_client_overall_aggregate.log", "latency_client_overall_average.log"]
for log in main_logs:
    common.localcmdbg("mv " + detailLogDir + "/" + log + " " + fullLogDir)

## this is only for plotting, need to change later
common.localcmd(
    "cp " + fullLogDir + "/throughput_client_overall_aggregate.log" + " " + fullLogDir + "/throughput_client_conservative_overall_aggregate.log")
common.localcmd(
    "cp " + fullLogDir + "/latency_client_overall_average.log" + " " + fullLogDir + "/latency_client_conservative_overall_average.log")


###########################################################################
# PLOTTING GRAPHS
###########################################################################
def get_file_in_path(path, fileName):
    for (dirname, dirs, files) in os.walk(path):
        for filename in files:
            if fileName in (filename):
                return path + filename


tput_plot = fullLogDir + "throughput.png"
tput_plot_cmd = ["MPLBACKEND=agg", common.throughputPlotting, "-i", detailLogDir, "-s", tput_plot]
tput_plotting_cmd = " ".join([str(val) for val in tput_plot_cmd])
# print tput_plotting_cmd
common.localcmdbg(tput_plotting_cmd)

###########################################################################
# clean up
if not common.LOCALHOST:
    common.localcmd(common.cleaner)
