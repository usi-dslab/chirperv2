#!/usr/bin/python

import sys
from os.path import isfile as file_exists

import common

socialNetworkDir = common.BIN_HOME + "/graphs/higgs/{}/metis/"
socialNetworkDir = common.BIN_HOME + "/graphs/twitter/{}/metis/"
socialNetworkDir = common.BIN_HOME + "/graphs/holme-kim/{}/metis/"
socialNetworkDir = common.BIN_HOME + "/graphs/holme-kim/{}/"
numUsers = 100000
numUsers = 81306
numUsers = 10000

loads = [100, 200, 400]
loads = [100]

preloadData = "False"
preloadData = "True"

partitionings = [4]
partitionings = [2, 4, 8]
partitionings = [2]

runCount = 1

data_type = ".txt"
data_type = ".json"

workloads = {
    # "mix": { "wp": 7.5, "wf": 3.75, "wu": 3.75, "wtl": 85},
    "purepost": {"wp": 1, "wf": 0, "wu": 0, "wtl": 0},
    # "follo"wunfollow"": {"wp": 0, "wf": 1.0, "wu": 1.0, "wtl": 0},
    # "puretimeline": {"wp": 0, "wf": 0, "wu": 0, "wtl": 1.0},
    # "purepost": {"wp": 0.9, "wf": 0.1, "wu": 0, "wtl": 0},
}

edge_cuts = ['0.01', '0.05', '0.10']
edge_cuts = ['twitter']
edge_cuts = ['0.10']


def run():
    for run in range(0, runCount):
        for numPartitions in partitionings:
            for edge_cut in edge_cuts:
                socialNetworkFile = socialNetworkDir.format(edge_cut) + "users_" + str(numUsers) + "_partitions_" + \
                                    str(numPartitions) + data_type

                if not file_exists(socialNetworkFile):
                    print('Social Network File doesn\'t exist ' + socialNetworkFile)
                    sys.exit(1)

                for wl in workloads:

                    # ####################################
                    # WORKLOAD
                    weightPost = workloads[wl]["wp"]
                    weightFollow = workloads[wl]["wf"]
                    weightUnfollow = workloads[wl]["wu"]
                    weightGetTimeline = workloads[wl]["wtl"]
                    workloadName = wl
                    # ####################################


                    for numClients in loads:
                        # numClients = numClients * (numPartitions/1.5)
                        # numClients = numClients * numPartitions
                        experimentCmd = ' '.join([str(val) for val in
                                                  # [common.onceRunner, "CHITTER", numClients * numPartitions,numPartitions,
                                                  [common.chirperAllInOne, numClients, numPartitions,
                                                   socialNetworkFile, preloadData, weightPost, weightFollow,
                                                   weightUnfollow, weightGetTimeline,
                                                   workloadName, edge_cut]])
                        print experimentCmd
                        common.localcmd(experimentCmd)


                        # def runTestBatch():


if __name__ == '__main__':
    run()



# 10k 10%
# 2p: [87.75, 12.25]
# 4p: [89.6, 8.91, 1.17, 0.32]
# 8p: [89.92, 8.3, 1.23, 0.25, 0.14, 0.07, 0.06, 0.03]

# twitter: 81k
# edgecut:
#   2p: 4.29,
#   4p: 6.79,
#   8p: 11.70
# 2p: [75.68, 24.32]
# 4p: [67.15, 23.14, 8.05, 1.67]
# 8p: [57.62, 22.95, 10.65, 4.93, 2.14, 1.0, 0.48, 0.22]
