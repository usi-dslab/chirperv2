#!/usr/bin/python



# ============================================
# ============================================
# preamble

import sys

import common
import systemConfigurer
from common import farg
from common import iarg
from common import sarg


# ./deployTestRunners.py /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/systemConfigs/minimal_system_config.json /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/systemConfigs/minimal_partitioning.json 2 1 10 /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/chirperV2/bin/graphs/holme-kim/0.01/users_10000_partitions_2.json 1 0 0 0

def usage():
    print "usage: " + sarg(0) + \
          "configFile partsFile numPartitions numClients minClientId socialNetworkFile weightPost weightFollow weightUnfollow weightGetTimeline"
    sys.exit(1)


# print sys.argv, len(sys.argv)
if len(sys.argv) not in [11]:
    usage()

# constants
NODE = 0
CLIENTS = 1

# deployment layout
configFile = sarg(1)
partsFile = sarg(2)
numPartitions = iarg(3)
numClients = iarg(4)
minCientId = iarg(5)
socialNetworkFile = sarg(6)

# command arguments
weightPost = farg(7)
weightFollow = farg(8)
weightUnfollow = farg(9)
weightGetTimeline = farg(10)

algargs = []
gathererNode = systemConfigurer.getGathererNode()

clientNodes = None

chirperConfig = systemConfigurer.generateSystemConfiguration(numPartitions, saveToFile=True)
clientNodes = systemConfigurer.getClientNodes(numPartitions)

clientId = minCientId
numPermits = 1
algargs = [clientId, configFile, partsFile, numPermits]

# ============================================
# ============================================
# begin

logDir = common.CHIRPER_LOG_CLIENTS

# commonargs  = [common.javabin, common.javacp, common.javaRunnerClass]
# commonargs += [common.duration, logDir, gathererNode, common.gathererPort]
# commonargs += [numPermits, socialNetworkFile]
# commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]
clientMapping = common.mapClientsToNodes(numClients, clientNodes)

for mapping in clientMapping:
    if mapping[CLIENTS] == 0:
        continue

    numPermits = mapping[CLIENTS]

    commonargs = [common.getJavaExec(mapping[NODE], 'CLIENT'), common.JAVA_CLASSPATH,
                  '-DHOSTNAME=' + str(clientId), common.CHIRPER_CLASS_TEST_RUNNER]
    commonargs += [numPermits, socialNetworkFile]
    commonargs += [gathererNode, common.gathererPort, logDir, common.EXPERIMENT_DURATION, common.EXPERIMENT_WARMUP_MS]
    commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]

    algargs[0] = str(clientId)
    algargs[3] = str(numPermits)
    cmdString = " ".join([str(val) for val in (commonargs + algargs)])
    # print cmdString
    common.sshcmdbg(mapping[NODE], cmdString)
    clientId += 1
