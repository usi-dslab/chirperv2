#!/usr/bin/python

import os

import common

# ./deployClient.py

client_id = "12345"
system_config_file = common.script_dir() + "/systemConfigs/minimal_system_config.json"
partitioning_file = common.script_dir() + "/systemConfigs/minimal_partitioning.json"
social_network_file = common.script_dir() + "/graphs/holme-kim/0.01/users_10000_partitions_2.json"
num_permits = "1"

# client_cmd = java_bin + app_classpath + client_class + client_id + config_file + partitioning_file + num_permits
client_cmd = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.CHIRPER_CLASS_CLIENT, client_id, system_config_file,
              partitioning_file, num_permits, social_network_file]
cmdString = " ".join([str(val) for val in client_cmd])
# print cmdString
os.system(cmdString)
